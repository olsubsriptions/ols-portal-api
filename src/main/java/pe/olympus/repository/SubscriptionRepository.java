package pe.olympus.repository;

import pe.olympus.model.GenericResponse;
import pe.olympus.model.Subscription;

public interface SubscriptionRepository {
	
	public GenericResponse createSubscription (Subscription subscription);
	public GenericResponse deleteSubscription (String subscriptionId);
	public boolean subscriptionExist (String indexKey);
}
