package pe.olympus.repository;

import pe.olympus.model.GenericResponse;

public interface UserSubscriptionRepository {
	
	public GenericResponse deleteUserSubscription(String userId, String subscriptionId);

}
