package pe.olympus.repository;

import pe.olympus.model.GenericResponse;
import pe.olympus.model.Register;
import pe.olympus.model.UserDTO;

public interface UserRepository {
	
	public GenericResponse registerUser(Register register);
	public GenericResponse registerGoogleUser(Register register);
	public GenericResponse validateUser(UserDTO userDTO);
	public GenericResponse validateRegister(UserDTO userDTO);
}
