package pe.olympus.repository;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.google.cloud.firestore.DocumentReference;
import com.google.cloud.firestore.Firestore;
import com.google.cloud.firestore.QuerySnapshot;
import com.google.cloud.firestore.WriteResult;
import com.google.gson.Gson;

import pe.olympus.constants.Constants;
import pe.olympus.constants.ResponseMessages;
import pe.olympus.model.GenericResponse;
import pe.olympus.model.Subscription;

import com.olympus.DataProvider;
import com.olympus.model.DataProviderResponse;
import com.olympus.model.ExecutionData;
import com.olympus.model.Parameter;
import com.olympus.model.Type;

@Repository
public class SubscriptionRepositoryImpl implements SubscriptionRepository {
	
	private final Logger logger = LoggerFactory.getLogger(this.getClass());
	
	private Firestore firebaseClient;
	private DataProvider dataProvider;
	private Gson gsonTemplate;
	
	@Autowired
	public SubscriptionRepositoryImpl(Firestore firebaseClient,
			DataProvider dataProvider,
			Gson gsonTemplate) {
		
		this.firebaseClient = firebaseClient;
		this.dataProvider = dataProvider;
		this.gsonTemplate = gsonTemplate;
	}

	@Override
	public GenericResponse createSubscription(Subscription subscription) {
		
		try {
			
			GenericResponse genericResponse = new GenericResponse();
			genericResponse.setStatus(false);
			genericResponse.setMessage(ResponseMessages.OPERACION_NOEXITOSA);
			
			Map<String, Object> subscriptionData = new HashMap<>();
			subscriptionData.put("b4322", subscription.getB4322());
			subscriptionData.put("b4323", subscription.getB4323());
			subscriptionData.put("b4324", subscription.getB4324());
			subscriptionData.put("b4325", subscription.getB4325());
			subscriptionData.put("f4461", subscription.getF4461());
			
			DocumentReference subscriptionRef = firebaseClient.collection(Constants.T_SUBSCRIPTIONS)
					.document(subscription.getB4321());
					
			WriteResult subscriptionWR = subscriptionRef.create(subscriptionData).get();
			
			if (subscriptionWR != null) {
				
				genericResponse.setStatus(true);
				genericResponse.setMessage(ResponseMessages.OPERACION_EXITOSA);
			}
					
			return genericResponse;
		} catch (Exception e) {			
			throw new RuntimeException(e.getMessage());
		}
	}

	@Override
	public boolean subscriptionExist(String indexKey) {
		
		try {
			
			QuerySnapshot querySnapshotS = firebaseClient.collection(Constants.T_SUBSCRIPTIONS)
					.whereEqualTo("b4325", indexKey)
					.get().get();
			
			return !querySnapshotS.isEmpty();
			
		} catch (Exception e) {			
			throw new RuntimeException(e.getMessage());
		}
	}

	@Override
	public GenericResponse deleteSubscription(String subscriptionId) {
		
		try {
			
			GenericResponse genericResponse = new GenericResponse();
			genericResponse.setStatus(false);
			genericResponse.setMessage(ResponseMessages.OPERACION_NOEXITOSA);			
			
			ExecutionData executionData = new ExecutionData("sp_b4320_d01",
					new Parameter("pb4321", subscriptionId, Type.VARCHAR));
			
			DataProviderResponse dpr = dataProvider.executeProcedure(executionData);
			
			if(dpr.getStatus()) {
				genericResponse.setStatus(true);
				genericResponse.setMessage(ResponseMessages.OPERACION_EXITOSA);
			}
			
			logger.info("SubscriptionRepository.deleteSubscription : deleteSubscription() is done.");
			logger.debug("SubscriptionRepository.deleteSubscription #genericResponse : {}", gsonTemplate.toJson(genericResponse));
			return genericResponse;
			
		} catch (Exception e) {
			logger.error("SubscriptionRepository.deleteSubscription #error : {}", e.getMessage());
			throw new RuntimeException(e.getMessage());
		}
	}

}
