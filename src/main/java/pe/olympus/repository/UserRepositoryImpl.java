package pe.olympus.repository;

import java.util.Date;
import java.util.List;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.google.firebase.FirebaseApp;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthException;
import com.google.firebase.auth.UserRecord;
import com.google.firebase.auth.UserRecord.CreateRequest;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;

import pe.olympus.constants.AuthenticationType;
import pe.olympus.constants.ResponseMessages;
import pe.olympus.constants.UserStatus;
import pe.olympus.model.Billing;
import pe.olympus.model.Company;
import pe.olympus.model.GenericResponse;
import pe.olympus.model.Organization;
import pe.olympus.model.Register;
import pe.olympus.model.UserDTO;
import pe.olympus.model.UserS;
import com.olympus.DataProvider;
import com.olympus.model.DataProviderResponse;
import com.olympus.model.ExecutionData;
import com.olympus.model.Parameter;
import com.olympus.model.Type;
import pe.olympus.service.CryptoService;

@Repository
public class UserRepositoryImpl implements UserRepository {
	
	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	private FirebaseApp firebaseApp;
	private CryptoService cryptoService;
	private OrganizationRepository organizationRepository;
	private UserSubscriptionRepository userSubscriptionRepository;
	private SubscriptionRepository subscriptionRepository;
	private DataProvider dataProvider;
	private Gson gsonTemplate;

	@Autowired
	public UserRepositoryImpl(CryptoService cryptoService,
			OrganizationRepository organizationRepository,
			UserSubscriptionRepository userSubscriptionRepository,
			SubscriptionRepository subscriptionRepository,
			FirebaseApp firebaseApp,
			DataProvider dataProvider, Gson gsonTemplate) {

		this.cryptoService = cryptoService;
		this.organizationRepository = organizationRepository;
		this.userSubscriptionRepository = userSubscriptionRepository;
		this.subscriptionRepository = subscriptionRepository;
		this.firebaseApp = firebaseApp;
		this.dataProvider = dataProvider;
		this.gsonTemplate = gsonTemplate;
	}

//	@Override
//	public GenericResponse validateUser(UserDTO userDTO) {
//		
//		GenericResponse genericResponse = new GenericResponse();
//		genericResponse.setStatus(false);
//		
//		if(userDTO.getUsername() == null || userDTO.getDomain() == null) {
//			genericResponse.setMessage("NO_LOGIN_DATA");
//			return genericResponse;
//		}
//		
//		try {
//			
//			genericResponse.setMessage("USER_NOT_FOUND");			
//			QuerySnapshot querySnapshotU = firebaseClient.collection(Constants.T_USERS)
//					.whereEqualTo("g6733", cryptoService.hash384(userDTO.getDomain().toLowerCase()))
//					.get().get();
//			
//			if(!querySnapshotU.isEmpty()) {
//				
//				DocumentSnapshot subscriptionDocument = firebaseClient.collection(Constants.T_SUBSCRIPTIONS)
//				.document(querySnapshotU.getDocuments().get(0).getString("b4321"))
//				.get().get();
//				
//				if(subscriptionDocument.exists()) {
//					
//					String indexKey = subscriptionDocument.getString("b4325");
//					String cipherData = userDTO.getUsername().toLowerCase() + '@' + userDTO.getDomain().toLowerCase();
//					
//					CipherRequest cipherRequest = cryptoService.getDefaultCipherRequest(cipherData, indexKey);
//					
//					ResponseObject response = cryptoService.encrypt(cipherRequest);
//					
//					if(response.getDataList().size() > 0) {
//						
//						QuerySnapshot querySnapshotU2 = firebaseClient.collection(Constants.T_USERS)
//								.whereEqualTo("g6732", response.getDataList().get(0))
//								.get().get();
//						if(!querySnapshotU2.isEmpty()) {
//							genericResponse.setStatus(true);
//							genericResponse.setMessage("USER_FOUND");
//							genericResponse.setUser(querySnapshotU2.getDocuments().get(0).getId() + "@olympus-cloud.com");
//						}
//					}					
//				}
//			}
//			
//			return genericResponse;
//			
//		} catch (Exception e) {
//			throw new RuntimeException(e.getMessage());
//		}
//	}

	@Override
	public GenericResponse validateUser(UserDTO userDTO) {

		GenericResponse genericResponse = new GenericResponse();
		genericResponse.setStatus(false);

		if (userDTO.getUsername() == null) {
			genericResponse.setMessage("NO_LOGIN_DATA");
			return genericResponse;
		}
		
		String domain = null;

		try {

			genericResponse.setMessage("USER_NOT_FOUND");
			
			switch (userDTO.getAuthType()) {
			case AuthenticationType.EMAIL:
				domain = userDTO.getUsername() + "@" + userDTO.getDomain();
				break;
			case AuthenticationType.DEFAULT:
				domain = userDTO.getDomain();
				break;
			}

			ExecutionData executionData = new ExecutionData("sp_g6730_s01",
					new Parameter("pg6733", cryptoService.hash384(domain).toLowerCase(), Type.VARCHAR));

			DataProviderResponse dpr = dataProvider.executeProcedure(executionData);
			
			if(dpr.getStatus()) {				
				List<UserS> userList = gsonTemplate.fromJson(dpr.getResult(), new TypeToken<List<UserS>>(){}.getType());
				UserS user = userList != null ? userList.get(0) : null;
				if(user != null) {
					genericResponse.setStatus(true);
					genericResponse.setMessage("USER_FOUND");
					genericResponse.setUser(user.getG6731() + "@olympus-cloud.com");
				}				
			} else {
				genericResponse.setMessage("DATABASE_COMUNICATION");
			}			
			
			logger.info("UserRepository.validateUser : validateUser() is done.");
			logger.debug("UserRepository.validateUser #genericResponse : {}", gsonTemplate.toJson(genericResponse));
			return genericResponse;

		} catch (Exception e) {
			logger.error("UserRepository.validateUser #error : {}", e.getMessage());
			throw new RuntimeException(e.getMessage());
		}
	}

	@Override
	public GenericResponse validateRegister(UserDTO userDTO) {

		GenericResponse genericResponse = new GenericResponse();
		genericResponse.setStatus(false);
		String organization = null;

		if (userDTO.isNull(userDTO.getAuthType())) {

			genericResponse.setMessage("NO_LOGIN_DATA");
			return genericResponse;
		}

		switch (userDTO.getAuthType()) {
		case AuthenticationType.EMAIL:
			organization = userDTO.getEmail();
			break;
		case AuthenticationType.DEFAULT:
			organization = userDTO.getDomain();
			break;
		}

		try {

			UserS user = getUser(organization);
			
			if(user != null) {
				if(user.getG6735() == UserStatus.VERIFICATION_PENDING) {
					UserRecord userRecord = FirebaseAuth.getInstance(firebaseApp).getUser(user.getG6731());					
					if(new Date().getTime() - userRecord.getUserMetadata().getCreationTimestamp() > 86400000) {
						// eliminar usuario
					}
				}
			} 
			
			if (organizationRepository.organizationExist(organization)) {

				genericResponse.setMessage("INVALID_ORGANIZATION");
			} else if (phoneRegistered(userDTO)) {

				genericResponse.setMessage("PHONE_ALREADY_EXIST");
			} else {

				genericResponse.setStatus(true);
				genericResponse.setMessage("VALID_REGISTER");
			}

			logger.info("UserRepository.validateRegister : validateRegister() is done.");
			logger.debug("UserRepository.validateRegister #genericResponse : {}", gsonTemplate.toJson(genericResponse));
			return genericResponse;

		} catch (Exception e) {
			logger.error("UserRepository.validateRegister #error : {}", e.getMessage());
			throw new RuntimeException(e.getMessage());
		}
	}

	@Override
	public GenericResponse registerUser(Register register) {

		GenericResponse genericResponse = new GenericResponse();
		genericResponse.setStatus(false);

		UserDTO loginData = register.getLogin();
		Company companyData = register.getCompany();
		String organization = null;

		if (loginData == null || loginData.isNull(loginData.getAuthType())) {

			genericResponse.setMessage("NO_LOGIN_DATA");
			return genericResponse;
		}

		if (companyData == null || companyData.isNull()) {

			genericResponse.setMessage("NO_COMPANY_DATA");
			return genericResponse;
		}

		switch (loginData.getAuthType()) {
		case AuthenticationType.EMAIL:
			organization = loginData.getEmail();
			break;
		case AuthenticationType.DEFAULT:
			organization = loginData.getDomain();
			break;
		}

		try {

			if (organizationRepository.organizationExist(organization)) {

				genericResponse.setMessage("INVALID_ORGANIZATION");
			} else if (phoneRegistered(loginData)) {

				genericResponse.setMessage("PHONE_ALREADY_EXIST");
			} else {
				genericResponse = registerFirebaseUser(register);
			}

			logger.info("UserRepository.registerUser : registerUser() is done.");
			logger.debug("UserRepository.registerUser #genericResponse : {}", gsonTemplate.toJson(genericResponse));
			return genericResponse;

		} catch (Exception e) {
			
			logger.error("UserRepository.registerUser #error : {}", e.getMessage());
			throw new RuntimeException(e.getMessage());
		}
	}

	@Override
	public GenericResponse registerGoogleUser(Register register) {

		GenericResponse genericResponse = new GenericResponse();
		genericResponse.setStatus(false);

		UserDTO loginData = register.getLogin();
		Company companyData = register.getCompany();

		if (loginData == null || loginData.isNull(0)) {

			genericResponse.setMessage("NO_LOGIN_DATA");
			return genericResponse;
		}

		if (companyData == null || companyData.isNull()) {

			genericResponse.setMessage("NO_COMPANY_DATA");
			return genericResponse;
		}

		try {

			if (phoneRegistered(loginData)) {

				genericResponse.setMessage("PHONE_ALREADY_EXIST");
			} else {

//				genericResponse = createGoogleUserData(register);
			}

			return genericResponse;

		} catch (Exception e) {
			throw new RuntimeException(e.getMessage());
		}
	}

//	private GenericResponse createUser(UserS user) {
//
//		try {
//
//			GenericResponse genericResponse = new GenericResponse();
//			genericResponse.setStatus(false);
//			genericResponse.setMessage(ResponseMessages.OPERACION_NOEXITOSA);
//
//			Map<String, Object> userData = new HashMap<>();
//			userData.put("g6732", user.getG6732());
//			userData.put("g6733", user.getG6733());
//			userData.put("g6734", user.getG6734());
//			userData.put("g6735", user.getG6735());
//			userData.put("b4321", user.getB4321());
//
//			DocumentReference userRef = firebaseClient.collection(Constants.T_USERS).document(user.getG6731());
//
//			WriteResult userWR = userRef.create(userData).get();
//
//			if (userWR != null) {
//
//				genericResponse.setStatus(true);
//				genericResponse.setMessage(ResponseMessages.OPERACION_EXITOSA);
//			}
//
//			return genericResponse;
//		} catch (Exception e) {
//			throw new RuntimeException(e.getMessage());
//		}
//	}
//
	private GenericResponse deleteUser(String userId) {

		try {

			GenericResponse genericResponse = new GenericResponse();
			genericResponse.setStatus(false);
			genericResponse.setMessage(ResponseMessages.OPERACION_NOEXITOSA);			
			
			ExecutionData executionData = new ExecutionData("sp_g6730_d01",
					new Parameter("pg6731", userId, Type.VARCHAR));
			
			DataProviderResponse dpr = dataProvider.executeProcedure(executionData);

			if(dpr.getStatus()) {
				genericResponse.setStatus(true);
				genericResponse.setMessage(ResponseMessages.OPERACION_EXITOSA);
			}
			
			logger.info("UserRepository.deleteUser : deleteUser() is done.");
			logger.debug("UserRepository.deleteUser #genericResponse : {}", gsonTemplate.toJson(genericResponse));
			return genericResponse;

		} catch (Exception e) {
			logger.error("UserRepository.deleteUser #error : {}", e.getMessage());
			throw new RuntimeException(e.getMessage());
		}
	}

	private GenericResponse registerFirebaseUser(Register register) {
		DataProviderResponse dpr = null;
		String userUID = null;
		String subscriptionUID = null;
		String organizationUID = null;
		GenericResponse tempGenericResponse = null;
		try {
			
			GenericResponse genericResponse = new GenericResponse();
			genericResponse.setStatus(false);
			genericResponse.setMessage(ResponseMessages.OPERACION_NOEXITOSA);

			UserDTO loginData = register.getLogin();
			Company companyData = register.getCompany();
			Billing billingData = register.getBilling();

			userUID = UUID.randomUUID().toString();
			String username = null;

			switch (loginData.getAuthType()) {
			case AuthenticationType.EMAIL:
				username = loginData.getEmail();
//				loginData.setDomain(username.split("@")[1].replace(".", ""));
				loginData.setDomain(username);
				break;
			case AuthenticationType.DEFAULT:
				username = loginData.getUsername().toLowerCase() + "@" + loginData.getDomain().toLowerCase();
				break;
			}

			CreateRequest createUserRequest = new CreateRequest();
			createUserRequest.setUid(userUID);
			createUserRequest.setEmail(userUID + "@olympus-cloud.com");
			createUserRequest.setEmailVerified(false);
			createUserRequest.setPassword(loginData.getPassword());
			createUserRequest.setDisplayName(username);
			createUserRequest.setDisabled(false);
			createUserRequest.setPhoneNumber(loginData.getCountry().getPrefix() + loginData.getPhoneNumber());

			UserRecord userRecord = FirebaseAuth.getInstance(firebaseApp).createUser(createUserRequest);
			if (userRecord != null) {			
				
				ExecutionData executionData = new ExecutionData("sp_b4320_s01");				
				dpr = dataProvider.executeProcedure(executionData);				
				
				if(dpr.getStatus()) {
					
					JsonArray resultArray = gsonTemplate.fromJson(dpr.getResult(), JsonArray.class);
					JsonObject resultObject = resultArray.get(0).getAsJsonObject();				
					
					organizationUID = UUID.randomUUID().toString();
					subscriptionUID = resultObject.get("b4321").getAsString();				
					String indexKey = resultObject.get("k1162").getAsString();
					String securityKey = cryptoService.getKey();
					String validationEmailUID = UUID.randomUUID().toString();
					
					executionData = new ExecutionData("sp_g6730_i01",
							new Parameter("pg6731", userUID, Type.VARCHAR),
							new Parameter("pg6732", cryptoService.defaultEncrypt(username, securityKey), Type.VARCHAR),
							new Parameter("pg6733", cryptoService.hash384(loginData.getDomain().toLowerCase()), Type.VARCHAR),
							new Parameter("pg6734", validationEmailUID, Type.VARCHAR),
							new Parameter("pg6735", UserStatus.VERIFICATION_PENDING, Type.INTEGER),
							new Parameter("pb4321", subscriptionUID, Type.VARCHAR),
							new Parameter("pf4461", organizationUID, Type.VARCHAR),
							new Parameter("pg6737", Integer.toString(loginData.getAuthType()), Type.VARCHAR),
							new Parameter("pg6736", loginData.getEmail(), Type.VARCHAR),
							new Parameter("b4322", cryptoService.getKey(), Type.VARCHAR),
							new Parameter("b4323", 1, Type.BIT),
							new Parameter("pk1161", securityKey, Type.VARCHAR),
							new Parameter("pk1162", indexKey, Type.VARCHAR));					
					dpr = dataProvider.executeProcedure(executionData);					
					resultArray = gsonTemplate.fromJson(dpr.getResult(), JsonArray.class);
					resultObject = resultArray.get(0).getAsJsonObject();
					
					if(dpr.getStatus() == true && resultObject.get("b4321").getAsString().equals(subscriptionUID)) {
						
						String primaryContactPhone = loginData.getCountry().getPrefix() + loginData.getPhoneNumber();
						
						Organization organization = new Organization();
						organization.setF4461(organizationUID);
						organization.setF4462(cryptoService.defaultEncrypt(companyData.getCompany(), securityKey));
						organization.setF4463(cryptoService.defaultEncrypt(companyData.getAddress1(), securityKey));
						organization.setF4464(cryptoService.defaultEncrypt(companyData.getAddress2(), securityKey));
						organization.setF4465(companyData.getCity());
						organization.setF4466(companyData.getProvince());
						organization.setF4467(companyData.getState());
						organization.setF4468(loginData.getCountry().getCode());
						organization.setF4469(cryptoService.defaultEncrypt(primaryContactPhone, securityKey));
						organization.setF4470(cryptoService.defaultEncrypt(companyData.getDocument(), securityKey));
						organization.setF4471(cryptoService.defaultEncrypt(loginData.getEmail(), securityKey));
						organization.setF4472(cryptoService.defaultEncrypt(billingData.getName(), securityKey));
						organization.setF4473(cryptoService.defaultEncrypt(billingData.getEmail(), securityKey));
						organization.setF4474(cryptoService.defaultEncrypt(billingData.getName2(), securityKey));
						organization.setF4475(cryptoService.defaultEncrypt(billingData.getEmail2(), securityKey));
						organization.setF4476(0);
						organization.setF4477(generateEmailVerificationLink(userUID + "@olympus-cloud.com"));
						
						GenericResponse orgGenericResponse= organizationRepository.createOrganization(organization);						
						
						if(orgGenericResponse.getStatus()) {
							genericResponse.setStatus(true);
							genericResponse.setMessage(ResponseMessages.OPERACION_EXITOSA);
							genericResponse.setData(validationEmailUID);
							genericResponse.setUser(userUID);
						} else {
							FirebaseAuth.getInstance(firebaseApp).deleteUser(userUID);
							tempGenericResponse = userUID != null && subscriptionUID != null ? userSubscriptionRepository.deleteUserSubscription(userUID, subscriptionUID) : tempGenericResponse;
							tempGenericResponse = subscriptionUID != null ? subscriptionRepository.deleteSubscription(subscriptionUID) : tempGenericResponse;
							tempGenericResponse = userUID != null ? deleteUser(userUID) : tempGenericResponse;
							genericResponse.setMessage(ResponseMessages.DATABASE_ERROR);
						}					
						
					} else {
						FirebaseAuth.getInstance(firebaseApp).deleteUser(userUID);						
						tempGenericResponse = userUID != null && subscriptionUID != null ? userSubscriptionRepository.deleteUserSubscription(userUID, subscriptionUID) : tempGenericResponse;
						tempGenericResponse = subscriptionUID != null ? subscriptionRepository.deleteSubscription(subscriptionUID) : tempGenericResponse;
						tempGenericResponse = userUID != null ? deleteUser(userUID) : tempGenericResponse;
						genericResponse.setMessage(ResponseMessages.DATABASE_ERROR);
					}
				} else {
					FirebaseAuth.getInstance(firebaseApp).deleteUser(userUID);
					genericResponse.setMessage(ResponseMessages.DATABASE_ERROR);
				}
			}

			logger.info("UserRepository.registerFirebaseUser : registerFirebaseUser() is done.");
			logger.debug("UserRepository.registerFirebaseUser #genericResponse : {}", gsonTemplate.toJson(genericResponse));
			return genericResponse;

		} catch (Exception e) {			
			
			try {
				tempGenericResponse = userUID != null && subscriptionUID != null ? userSubscriptionRepository.deleteUserSubscription(userUID, subscriptionUID) : tempGenericResponse;
				tempGenericResponse = subscriptionUID != null ? subscriptionRepository.deleteSubscription(subscriptionUID) : tempGenericResponse;
				tempGenericResponse = userUID != null ? deleteUser(userUID) : tempGenericResponse;
				tempGenericResponse = organizationUID != null ? organizationRepository.deleteOrganization(organizationUID) : tempGenericResponse;
				FirebaseAuth.getInstance(firebaseApp).deleteUser(userUID);
			} catch (FirebaseAuthException e1) {
				
				throw new RuntimeException(e1.getMessage());
			}
			logger.error("UserRepository.registerFirebaseUser #error : {}", e.getMessage());
			throw new RuntimeException(e.getMessage());
		}
	}

	private boolean phoneRegistered(UserDTO userDTO) {

		try {
			String phoneNumber = userDTO.getCountry().getPrefix() + userDTO.getPhoneNumber();
			FirebaseAuth.getInstance(firebaseApp).getUserByPhoneNumber(phoneNumber);
			
			logger.info("UserRepository.phoneRegistered : phoneRegistered() is done.");
			logger.debug("UserRepository.phoneRegistered #response : {}", true);
			return true;
		} catch (Exception e) {
			logger.info("UserRepository.phoneRegistered : phoneRegistered() is done.");
			logger.debug("UserRepository.phoneRegistered #response : {}", false);
			return false;
		}
	}

	private String generateEmailVerificationLink(String email) {

		try {
			String emailLink = FirebaseAuth.getInstance(firebaseApp).generateEmailVerificationLink(email);
			
			logger.info("UserRepository.generateEmailVerificationLink : generateEmailVerificationLink() is done.");
			logger.debug("UserRepository.generateEmailVerificationLink #emailLink : {}", emailLink);
			return emailLink;
		} catch (Exception e) {
			
			logger.error("UserRepository.generateEmailVerificationLink #error : {}", e.getMessage());
			throw new RuntimeException(e.getMessage());
		}

	}
	
	private UserS getUser(String organization) {
		
		try {			
			UserS user = new UserS();
			ExecutionData executionData = new ExecutionData("sp_g6730_s01",
					new Parameter("pg6733", cryptoService.hash384(organization).toLowerCase(), Type.VARCHAR));

			DataProviderResponse dpr = dataProvider.executeProcedure(executionData);
			List<UserS> userList = gsonTemplate.fromJson(dpr.getResult(), new TypeToken<List<UserS>>(){}.getType());
			user = userList != null ? userList.get(0) : null;
			
			logger.info("UserRepository.getUser : getUser() is done.");
			logger.debug("UserRepository.getUser #user : {}", gsonTemplate.toJson(user));
			return user;
			
		} catch (Exception e) {
			logger.error("UserRepository.getUser #error : {}", e.getMessage());
			throw new RuntimeException(e.getMessage());
		}		
	}
}
