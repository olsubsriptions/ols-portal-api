package pe.olympus.repository;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.olympus.DataProvider;
import com.olympus.model.DataProviderResponse;
import com.olympus.model.ExecutionData;
import com.olympus.model.Parameter;
import com.olympus.model.Type;

import pe.olympus.constants.ResponseMessages;
import pe.olympus.model.GenericResponse;
import pe.olympus.model.Organization;
import pe.olympus.service.CryptoService;

@Repository
public class OrganizationRepositoryImpl implements OrganizationRepository {
	
	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	private CryptoService cryptoService;
	private DataProvider dataProvider;
	private Gson gsonTemplate;
	
	@Autowired
	public OrganizationRepositoryImpl (
			CryptoService cryptoService,
			DataProvider dataProvider,
			Gson gsonTemplate) {
		
		this.cryptoService = cryptoService;
		this.dataProvider = dataProvider;
		this.gsonTemplate = gsonTemplate;
	}
	
	@Override
	public boolean organizationExist(String domain) {
		
		try {
			Boolean result = true;
//			ProviderResponse providerResponse = new ProviderResponse();
			
			ExecutionData executionData = new ExecutionData("sp_g6730_s02",
					new Parameter("pg6733", cryptoService.hash384(domain).toLowerCase(), Type.VARCHAR));
			
			DataProviderResponse dpr = dataProvider.executeProcedure(executionData);
				
			JsonArray resultArray = gsonTemplate.fromJson(dpr.getResult(), JsonArray.class);
			JsonObject resultObject = resultArray != null ? resultArray.get(0).getAsJsonObject() : null;
			
			if(resultObject != null) {
				result = resultObject.get("response").getAsInt() == 1 ? true : false;
			}			
			
			logger.info("OrganizationRepository.organizationExist : organizationExist() is done.");
			logger.debug("OrganizationRepository.organizationExist #result : {}", result);
			return result;
			
		} catch (Exception e) {
			
			logger.error("OrganizationRepository.organizationExist #error : {}", e.getMessage());
			throw new RuntimeException(e.getMessage());
		}	
	}
	
	@Override
	public GenericResponse createOrganization(Organization organization) {
		
		try {
			
			GenericResponse genericResponse = new GenericResponse();
			genericResponse.setStatus(false);
			genericResponse.setMessage(ResponseMessages.OPERACION_NOEXITOSA);
						
			ExecutionData executionData = new ExecutionData("sp_f4460_i01",
					new Parameter("pf4461", organization.getF4461(), Type.VARCHAR),
					new Parameter("pf4462", organization.getF4462(), Type.VARCHAR),
					new Parameter("pf4463", organization.getF4463(), Type.VARCHAR),
					new Parameter("pf4464", organization.getF4464(), Type.VARCHAR),
					new Parameter("pf4465", organization.getF4465(), Type.VARCHAR),
					new Parameter("pf4466", organization.getF4466(), Type.VARCHAR),
					new Parameter("pf4467", organization.getF4467(), Type.VARCHAR),
					new Parameter("pf4468", organization.getF4468(), Type.VARCHAR),
					new Parameter("pf4469", organization.getF4469(), Type.VARCHAR),
					new Parameter("pf4470", organization.getF4470(), Type.VARCHAR),
					new Parameter("pf4471", organization.getF4471(), Type.VARCHAR),
					new Parameter("pf4472", organization.getF4472(), Type.VARCHAR),
					new Parameter("pf4473", organization.getF4473(), Type.VARCHAR),
					new Parameter("pf4474", organization.getF4474(), Type.VARCHAR),
					new Parameter("pf4475", organization.getF4475(), Type.VARCHAR),
					new Parameter("pf4476", organization.getF4476(), Type.INTEGER),
					new Parameter("pf4477", organization.getF4477(), Type.VARCHAR));
			
			DataProviderResponse dpr = dataProvider.executeProcedure(executionData);
			
			if(dpr.getStatus()) {
				genericResponse.setStatus(true);
				genericResponse.setMessage(ResponseMessages.OPERACION_EXITOSA);
			}
			
			logger.info("OrganizationRepository.createOrganization : createOrganization() is done.");
			logger.debug("OrganizationRepository.createOrganization #genericResponse : {}", gsonTemplate.toJson(genericResponse));
			return genericResponse;
		} catch (Exception e) {
			logger.error("OrganizationRepository.createOrganization #error : {}", e.getMessage());
			throw new RuntimeException(e.getMessage());
		}
	}

	@Override
	public GenericResponse deleteOrganization(String OrganizationId) {
		try {
			
			GenericResponse genericResponse = new GenericResponse();
			genericResponse.setStatus(false);
			genericResponse.setMessage(ResponseMessages.OPERACION_NOEXITOSA);			
			
			ExecutionData executionData = new ExecutionData("sp_f4460_d01",
					new Parameter("pf4461", OrganizationId, Type.VARCHAR));
			
			DataProviderResponse dpr = dataProvider.executeProcedure(executionData);
			
			if(dpr.getStatus()) {
				genericResponse.setStatus(true);
				genericResponse.setMessage(ResponseMessages.OPERACION_EXITOSA);
			}
			
			logger.info("OrganizationRepository.deleteOrganization : deleteOrganization() is done.");
			logger.debug("OrganizationRepository.deleteOrganization #genericResponse : {}", gsonTemplate.toJson(genericResponse));
			return genericResponse;
			
		} catch (Exception e) {
			logger.error("OrganizationRepository.deleteOrganization #error : {}", e.getMessage());
			throw new RuntimeException(e.getMessage());
		}
	}
}
