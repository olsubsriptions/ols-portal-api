package pe.olympus.repository;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.google.gson.Gson;

import pe.olympus.constants.ResponseMessages;
import pe.olympus.model.GenericResponse;
import com.olympus.DataProvider;
import com.olympus.model.DataProviderResponse;
import com.olympus.model.ExecutionData;
import com.olympus.model.Parameter;
import com.olympus.model.Type;

@Repository
public class UserSubscriptionRepositoryImpl implements UserSubscriptionRepository{
	
	private final Logger logger = LoggerFactory.getLogger(this.getClass());
	private DataProvider dataProvider;
	private Gson gsonTemplate;
	
	@Autowired
	public UserSubscriptionRepositoryImpl(
			DataProvider dataProvider,
			Gson gsonTemplate) {
		
		this.dataProvider = dataProvider;
		this.gsonTemplate = gsonTemplate;
	}

	@Override
	public GenericResponse deleteUserSubscription(String userId, String subscriptionId) {
		
		try {
			GenericResponse genericResponse = new GenericResponse();
			genericResponse.setStatus(false);
			genericResponse.setMessage(ResponseMessages.OPERACION_NOEXITOSA);			
						
			ExecutionData executionData = new ExecutionData("sp_k1160_d01",
					new Parameter("pg6731", userId, Type.VARCHAR),
					new Parameter("pb4321", subscriptionId, Type.VARCHAR));
			
			DataProviderResponse dpr = dataProvider.executeProcedure(executionData);
			
			if(dpr.getStatus()) {
				genericResponse.setStatus(true);
				genericResponse.setMessage(ResponseMessages.OPERACION_EXITOSA);
			}
			
			logger.info("UserSubscriptionRepository.deleteUserSubscription : deleteUserSubscription() is done.");
			logger.debug("UserSubscriptionRepository.deleteUserSubscription #genericResponse : {}", gsonTemplate.toJson(genericResponse));
			return genericResponse;
			
		} catch (Exception e) {
			logger.error("UserSubscriptionRepository.deleteUserSubscription #error : {}", e.getMessage());
			throw new RuntimeException(e.getMessage());
		}
	}

}
