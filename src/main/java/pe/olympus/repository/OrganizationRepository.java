package pe.olympus.repository;

import pe.olympus.model.GenericResponse;
import pe.olympus.model.Organization;

public interface OrganizationRepository {

	public boolean organizationExist(String domain);
	public GenericResponse createOrganization(Organization organization);
	public GenericResponse deleteOrganization(String OrganizationId);
}
