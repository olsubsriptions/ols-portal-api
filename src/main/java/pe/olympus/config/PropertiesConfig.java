package pe.olympus.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class PropertiesConfig {
	
	@Value("${cryptosvc.url}")
	private String cryptosvcUrl;	
	@Value("${provider}")
	private String provider;
	@Value("${db.server}")
	private String dbServer;
	@Value("${db.port}")
	private String dbPort;
	@Value("${db.name}")
	private String dbName;
	@Value("${db.user}")
	private String dbUser;
	@Value("${db.secret}")
	private String dbSecret;
	
	public String getCryptosvcUrl() {
		return cryptosvcUrl;
	}
	public void setCryptosvcUrl(String cryptosvcUrl) {
		this.cryptosvcUrl = cryptosvcUrl;
	}	
	public String getProvider() {
		return provider;
	}
	public void setProvider(String provider) {
		this.provider = provider;
	}
	public String getDbServer() {
		return dbServer;
	}
	public void setDbServer(String dbServer) {
		this.dbServer = dbServer;
	}
	public String getDbPort() {
		return dbPort;
	}
	public void setDbPort(String dbPort) {
		this.dbPort = dbPort;
	}
	public String getDbName() {
		return dbName;
	}
	public void setDbName(String dbName) {
		this.dbName = dbName;
	}
	public String getDbUser() {
		return dbUser;
	}
	public void setDbUser(String dbUser) {
		this.dbUser = dbUser;
	}
	public String getDbSecret() {
		return dbSecret;
	}
	public void setDbSecret(String dbSecret) {
		this.dbSecret = dbSecret;
	}	
}
