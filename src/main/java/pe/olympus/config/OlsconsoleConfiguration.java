package pe.olympus.config;

import java.io.IOException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.auth.oauth2.GoogleCredentials;
import com.google.cloud.firestore.Firestore;
import com.google.cloud.firestore.FirestoreOptions;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.olympus.DataProvider;
import com.olympus.model.ConnectionData;

@Configuration
public class OlsconsoleConfiguration {
	
	@Autowired
	private PropertiesConfig propertiesConfig;
	
	@Bean
	public Firestore firebaseClient() throws IOException {
		FirestoreOptions firestoreOptions =
				FirestoreOptions.getDefaultInstance().toBuilder()
				.setCredentials(GoogleCredentials.getApplicationDefault())
				.setProjectId("subcriptions-2020-02")
				.build();
		Firestore fireStore = firestoreOptions.getService();
		return fireStore;		
	}
	
	@Bean 
	public FirebaseApp firebaseApp() throws Exception {
		
		FirebaseApp firebaseApp = null;
		
		FirebaseOptions options = new FirebaseOptions.Builder() 
			    .setCredentials(GoogleCredentials.getApplicationDefault())
			    .setProjectId("subcriptions-2020-02")
			    .build();
		 
		boolean hasBeenInitialized = false;
		List<FirebaseApp> firebaseApps = FirebaseApp.getApps();
		for(FirebaseApp app : firebaseApps){
		    if(app.getName().equals(FirebaseApp.DEFAULT_APP_NAME)){
		        hasBeenInitialized = true;
		        firebaseApp = app;
		    }
		}

		if(!hasBeenInitialized) {
		    firebaseApp = FirebaseApp.initializeApp(options);
		}
		
		return firebaseApp;
	}
	
	@Bean
	public DriverManagerDataSource dataSource() {
      DriverManagerDataSource dataSource = new DriverManagerDataSource();
      dataSource.setDriverClassName("com.mysql.cj.jdbc.Driver");
      return dataSource;
	}

	@Bean
	public JdbcTemplate jdbcTemplate() {
      JdbcTemplate jdbcTemplate = new JdbcTemplate();
      jdbcTemplate.setDataSource(dataSource());
      return jdbcTemplate;
	}
	
	@Bean 
	public FirebaseAuth firebaseAdmin() throws Exception {
		
		FirebaseAuth firebaseAuth = FirebaseAuth.getInstance();		
		return firebaseAuth;
	}
	
	@Bean
	public ObjectMapper mapper() {		
		return new ObjectMapper();		
	}
	
	@Bean
	public RestTemplate rest() {
		return new RestTemplate();
	}
	
	@Bean
	public Gson gsonTemplate() {
		return new GsonBuilder().disableHtmlEscaping().setDateFormat("MMMM dd, yyyy hh:mm:ss a").create();
	}
	
	@Bean
	public DataProvider dataProvider() {
		
		ConnectionData connectionData = new ConnectionData();
		connectionData.setServer(propertiesConfig.getDbServer());
		connectionData.setPort(propertiesConfig.getDbPort());
		connectionData.setDatabase(propertiesConfig.getDbName());
		connectionData.setUser(propertiesConfig.getDbUser());
		connectionData.setPassword(propertiesConfig.getDbSecret());
		
		DataProvider dataProvider = new DataProvider(connectionData, propertiesConfig.getProvider());
		return dataProvider;
	}
}
