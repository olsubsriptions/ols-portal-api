package pe.olympus.model;

public class UserS {
	
	private String g6731; // ouid
	private String g6732; // username
	private String g6733; // domain
	private String b4321; // suscriptionID
	private String g6734; // verify link
	private int g6735; // status
	
	public String getG6731() {
		return g6731;
	}
	public void setG6731(String g6731) {
		this.g6731 = g6731;
	}
	public String getG6732() {
		return g6732;
	}
	public void setG6732(String g6732) {
		this.g6732 = g6732;
	}
	public String getG6733() {
		return g6733;
	}
	public void setG6733(String g6733) {
		this.g6733 = g6733;
	}	
	public String getG6734() {
		return g6734;
	}
	public void setG6734(String g6734) {
		this.g6734 = g6734;
	}
	public int getG6735() {
		return g6735;
	}
	public void setG6735(int g6735) {
		this.g6735 = g6735;
	}
	public String getB4321() {
		return b4321;
	}
	public void setB4321(String b4321) {
		this.b4321 = b4321;
	}
	@Override
	public String toString() {
		return "UserS [g6731=" + g6731 + ", g6732=" + g6732 + ", g6733=" + g6733 + ", b4321=" + b4321 + "]";
	}	
}
