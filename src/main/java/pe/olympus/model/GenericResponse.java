package pe.olympus.model;

public class GenericResponse {
	
	private boolean status;
	private String message;
	private String user;
	private String data;
	
	public boolean getStatus() {
		return status;
	}
	public void setStatus(boolean status) {
		this.status = status;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getUser() {
		return user;
	}
	public void setUser(String user) {
		this.user = user;
	}
	
	public String getData() {
		return data;
	}
	public void setData(String data) {
		this.data = data;
	}
	@Override
	public String toString() {
		return "GenericResponse [status=" + status + ", message=" + message + ", user=" + user + ", data=" + data + "]";
	}
	
}
