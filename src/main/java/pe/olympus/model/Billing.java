package pe.olympus.model;

public class Billing {
	
	private String name;
	private String email;
	private String name2;
	private String email2;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getName2() {
		return name2;
	}
	public void setName2(String name2) {
		this.name2 = name2;
	}
	public String getEmail2() {
		return email2;
	}
	public void setEmail2(String email2) {
		this.email2 = email2;
	}
	@Override
	public String toString() {
		return "Billing [name=" + name + ", email=" + email + ", name2=" + name2 + ", email2=" + email2 + "]";
	}	
}
