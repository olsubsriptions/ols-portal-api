package pe.olympus.model;

public class UserDTO {
	private String userId;
	private String username;
	private String domain;
	private String email;
	private Country country;
	private String phoneNumber;
	private String password;
	private String checkPassword;
	private String validationPhoneCode;
	private int authType;

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getDomain() {
		return domain;
	}

	public void setDomain(String domain) {
		this.domain = domain;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Country getCountry() {
		return country;
	}

	public void setCountry(Country country) {
		this.country = country;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getCheckPassword() {
		return checkPassword;
	}

	public void setCheckPassword(String checkPassword) {
		this.checkPassword = checkPassword;
	}

	public String getValidationPhoneCode() {
		return validationPhoneCode;
	}

	public void setValidationPhoneCode(String validationPhoneCode) {
		this.validationPhoneCode = validationPhoneCode;
	}

	public int getAuthType() {
		return authType;
	}

	public void setAuthType(int authType) {
		this.authType = authType;
	}

	@Override
	public String toString() {
		return "UserDTO [userId=" + userId + ", username=" + username + ", domain=" + domain + ", email=" + email
				+ ", country=" + country + ", phoneNumber=" + phoneNumber + ", password=" + password
				+ ", checkPassword=" + checkPassword + ", validationPhoneCode=" + validationPhoneCode + ", authType="
				+ authType + "]";
	}

	public boolean isNull(int authType) {

		switch (authType) {
		case 0:
			return true;
		case 2:
			if (this.username == null)
				return true;
			if (this.domain == null)
				return true;
			break;
		}

		if (this.email == null)
			return true;
		if (this.country == null)
			return true;
		if (this.phoneNumber == null)
			return true;
		if (this.password == null)
			return true;
		return false;
	}
}
