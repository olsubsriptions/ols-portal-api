package pe.olympus.model;

public class Company {
	
	private String company;
	private String document;
	private String state;
	private String province;
	private String city;
	private String address1;
	private String address2;
	
	public String getCompany() {
		return company;
	}
	public void setCompany(String company) {
		this.company = company;
	}
	public String getDocument() {
		return document;
	}
	public void setDocument(String document) {
		this.document = document;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getProvince() {
		return province;
	}
	public void setProvince(String province) {
		this.province = province;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getAddress1() {
		return address1;
	}
	public void setAddress1(String address1) {
		this.address1 = address1;
	}
	public String getAddress2() {
		return address2;
	}
	public void setAddress2(String address2) {
		this.address2 = address2;
	}
	@Override
	public String toString() {
		return "Company [company=" + company + ", document=" + document + ", state=" + state + ", province=" + province
				+ ", city=" + city + ", address1=" + address1 + ", address2=" + address2 + "]";
	}
	
	public boolean isNull() {
		if(this.company == null) return true;
		if(this.document == null) return true;
		if(this.state == null) return true;
		if(this.province == null) return true;
		if(this.city == null) return true;
		if(this.address1 == null) return true;
		if(this.address2 == null) return true;
		return false;
	}
}
