package pe.olympus.model;

public class Organization {
	
	private String f4461; // uid
	private String f4462; // organizationName
	private String f4463; // organizationAdress1
	private String f4464; // organizationAdress2
	private String f4465; // city
	private String f4466; // province
	private String f4467; // state
	private String f4468; // countryPrefix
	private String f4469; // primaryContactPhone
	private String f4470; // taxId
	private String f4471; // primaryContactEmail
	private String f4472; // administrativeContact
	private String f4473; // administrativeEmail
	private String f4474; // billingContact
	private String f4475; // billinEmail
	private int f4476; // status
	private String f4477; // firebase email verification link
	
	public String getF4461() {
		return f4461;
	}
	public void setF4461(String f4461) {
		this.f4461 = f4461;
	}
	public String getF4462() {
		return f4462;
	}
	public void setF4462(String f4462) {
		this.f4462 = f4462;
	}
	public String getF4463() {
		return f4463;
	}
	public void setF4463(String f4463) {
		this.f4463 = f4463;
	}
	public String getF4464() {
		return f4464;
	}
	public void setF4464(String f4464) {
		this.f4464 = f4464;
	}
	public String getF4465() {
		return f4465;
	}
	public void setF4465(String f4465) {
		this.f4465 = f4465;
	}
	public String getF4466() {
		return f4466;
	}
	public void setF4466(String f4466) {
		this.f4466 = f4466;
	}
	public String getF4467() {
		return f4467;
	}
	public void setF4467(String f4467) {
		this.f4467 = f4467;
	}
	public String getF4468() {
		return f4468;
	}
	public void setF4468(String f4468) {
		this.f4468 = f4468;
	}
	public String getF4469() {
		return f4469;
	}
	public void setF4469(String f4469) {
		this.f4469 = f4469;
	}
	public String getF4470() {
		return f4470;
	}
	public void setF4470(String f4470) {
		this.f4470 = f4470;
	}
	public String getF4471() {
		return f4471;
	}
	public void setF4471(String f4471) {
		this.f4471 = f4471;
	}
	public String getF4472() {
		return f4472;
	}
	public void setF4472(String f4472) {
		this.f4472 = f4472;
	}
	public String getF4473() {
		return f4473;
	}
	public void setF4473(String f4473) {
		this.f4473 = f4473;
	}
	public String getF4474() {
		return f4474;
	}
	public void setF4474(String f4474) {
		this.f4474 = f4474;
	}
	public String getF4475() {
		return f4475;
	}
	public void setF4475(String f4475) {
		this.f4475 = f4475;
	}
	public int getF4476() {
		return f4476;
	}
	public void setF4476(int f4476) {
		this.f4476 = f4476;
	}
	public String getF4477() {
		return f4477;
	}
	public void setF4477(String f4477) {
		this.f4477 = f4477;
	}
}
