package pe.olympus.model;

public class Subscription {
	
	private String b4321; // subscriptionId
	private String f4461; // organizationId
	private String b4322; // securityKey
	private boolean b4323; // state
	private String b4324; // derivatyKey
	private String b4325; // indexKey
	
	public String getB4321() {
		return b4321;
	}
	public void setB4321(String b4321) {
		this.b4321 = b4321;
	}
	public String getF4461() {
		return f4461;
	}
	public void setF4461(String f4461) {
		this.f4461 = f4461;
	}
	public String getB4322() {
		return b4322;
	}
	public void setB4322(String b4322) {
		this.b4322 = b4322;
	}
	public boolean getB4323() {
		return b4323;
	}
	public void setB4323(boolean b4323) {
		this.b4323 = b4323;
	}
	public String getB4324() {
		return b4324;
	}
	public void setB4324(String b4324) {
		this.b4324 = b4324;
	}
	public String getB4325() {
		return b4325;
	}
	public void setB4325(String b4325) {
		this.b4325 = b4325;
	}
	
	@Override
	public String toString() {
		return "Subscription [b4321=" + b4321 + ", f4461=" + f4461 + ", b4322=" + b4322 + ", b4323=" + b4323
				+ ", b4324=" + b4324 + ", b4325=" + b4325 + "]";
	}
	
}
