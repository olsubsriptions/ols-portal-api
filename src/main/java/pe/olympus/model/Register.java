package pe.olympus.model;

public class Register {

	private UserDTO login;
	private Company company;
	private Billing billing;
	
	public UserDTO getLogin() {
		return login;
	}
	public void setLogin(UserDTO login) {
		this.login = login;
	}
	public Company getCompany() {
		return company;
	}
	public void setCompany(Company company) {
		this.company = company;
	}
	public Billing getBilling() {
		return billing;
	}
	public void setBilling(Billing billing) {
		this.billing = billing;
	}
	@Override
	public String toString() {
		return "Register [login=" + login + ", company=" + company + ", billing=" + billing + "]";
	}
}
