package pe.olympus.service;

import java.util.ArrayList;
import java.util.List;
import java.util.zip.CRC32;

import org.apache.commons.codec.digest.DigestUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.google.gson.Gson;

import pe.olympus.config.PropertiesConfig;
import pe.olympus.constants.Constants;
import pe.olympus.model.CipherParamRequest;
import pe.olympus.model.CipherRequest;
import pe.olympus.model.Crypto;
import pe.olympus.model.ResponseObject;

@Service
public class CryptoService {
	
	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	private PropertiesConfig propertiesConfig;
	private RestTemplate restTemplate;
	private String url;
	private Gson gsonTemplate;

	@Autowired
	public CryptoService(PropertiesConfig propertiesConfig, RestTemplate restTemplate, Gson gsonTemplate) {

		this.propertiesConfig = propertiesConfig;
		this.restTemplate = restTemplate;
		this.gsonTemplate = gsonTemplate;
		this.url = this.propertiesConfig.getCryptosvcUrl();
	}

	public Crypto generateConfigurationData(Crypto crypto) {
		try {

			crypto = restTemplate.postForObject(this.url + "/v1/crypto/kDataGenerator", crypto, Crypto.class);

			logger.info("CryptoService.generateConfigurationData : generateConfigurationData() is done.");
			logger.debug("CryptoService.generateConfigurationData #crypto : {}", gsonTemplate.toJson(crypto));
			return crypto;

		} catch (Exception e) {
			logger.error("CryptoService.generateConfigurationData #error : {}", e.getMessage());
			throw new RuntimeException(e.getMessage());
		}
	}

	public String getKey() {

		try {

			CipherParamRequest cipher = new CipherParamRequest();
			cipher.setAlgorithm(Constants.ALGORITHM_AES256);
			cipher.setCharset(Constants.CHARSET_UTF8);

			Crypto crypto = new Crypto();
			crypto.setKeyType(Constants.KEY_SK);
			crypto.setCipher(cipher);
			crypto = restTemplate.postForObject(this.url + "/v1/crypto/keyGenerator", crypto, Crypto.class);

			logger.info("CryptoService.getKey : getKey() is done.");
			logger.debug("CryptoService.getKey #crypto.getEncKey() : {}", crypto.getEncKey());
			return crypto.getEncKey();

		} catch (Exception e) {
			logger.error("CryptoService.getKey #error : {}", e.getMessage());
			throw new RuntimeException(e.getMessage());
		}
	}

	public ResponseObject encrypt(CipherRequest cipherRequest) {

		try {

			if (cipherRequest.getDataList() == null || cipherRequest.getCipher() == null) {
				return null;
			}
			ResponseObject response = restTemplate.postForObject(this.url + "/v1/crypto/encrypt", cipherRequest,
					ResponseObject.class);

			logger.info("CryptoService.encrypt : encrypt() is done.");
			logger.debug("CryptoService.encrypt #response : {}", gsonTemplate.toJson(response));
			return response;

		} catch (Exception e) {
			logger.error("CryptoService.encrypt #error : {}", e.getMessage());
			throw new RuntimeException(e.getMessage());
		}
	}

	public ResponseObject decrypt(CipherRequest cipherRequest) {

		try {

			if (cipherRequest.getDataList() == null || cipherRequest.getCipher() == null) {
				return null;
			}
			ResponseObject response = restTemplate.postForObject(this.url + "/v1/crypto/decrypt", cipherRequest,
					ResponseObject.class);

			logger.info("CryptoService.decrypt : decrypt() is done.");
			logger.debug("CryptoService.decrypt #response : {}", gsonTemplate.toJson(response));
			return response;

		} catch (Exception e) {
			logger.error("CryptoService.decrypt #error : {}", e.getMessage());
			throw new RuntimeException(e.getMessage());
		}
	}

	public CipherRequest getDefaultCipherRequest(String data, String securityKey) {

		try {

			CipherParamRequest cipherParam = new CipherParamRequest();
			cipherParam.setAlgorithm(Constants.ALGORITHM_AES256);
			cipherParam.setCharset(Constants.CHARSET_UTF8);

			List<String> dataList = new ArrayList<String>();
			dataList.add(data);

			CipherRequest cipherRequest = new CipherRequest();
			cipherRequest.setCipher(cipherParam);
			cipherRequest.setDataList(dataList);
			cipherRequest.setSecurityKey(securityKey);

			logger.info("CryptoService.getDefaultCipherRequest : getDefaultCipherRequest() is done.");
			logger.debug("CryptoService.getDefaultCipherRequest #cipherRequest : {}",
					gsonTemplate.toJson(cipherRequest));
			return cipherRequest;

		} catch (Exception e) {
			logger.error("CryptoService.getDefaultCipherRequest #error : {}", e.getMessage());
			throw new RuntimeException(e.getMessage());
		}
	}

	public String hash384(String text) {

		logger.info("CryptoService.hash384 : hash384() is done.");
		return DigestUtils.sha384Hex(text);
	}
	
	public String crc32(String text) {
		
		CRC32 crc32 = new CRC32();
		crc32.update(text.getBytes());
		
		logger.info("CryptoService.crc32 : crc32() is done.");
		return Long.toHexString(crc32.getValue());
	}
	
	public String defaultDecrypt(String text, String securityKey) {
		
		try {
			
			CipherRequest cipherRequest = getDefaultCipherRequest(text, securityKey);			
			ResponseObject response = decrypt(cipherRequest);
			
			logger.info("CryptoService.defaultDecrypt : defaultDecrypt() is done.");
			return response.getDataList().get(0);
			
		} catch (Exception e) {
			
			logger.error("CryptoService.defaultDecrypt #error : {}", e.getMessage());
			throw new RuntimeException(e.getMessage());
		}
		
	}
	
	public String defaultEncrypt(String text, String securityKey) {

		try {

			CipherRequest cipherRequest = getDefaultCipherRequest(text, securityKey);
			ResponseObject response = encrypt(cipherRequest);
			
			logger.info("CryptoService.defaultEncrypt : defaultEncrypt() is done.");
			return response.getDataList().get(0);

		} catch (Exception e) {
			
			logger.error("CryptoService.defaultEncrypt #error : {}", e.getMessage());
			throw new RuntimeException(e.getMessage());
		}
	}

//	public String encrypt(String text, String key) {
//		if (text == null || key == null) {
//		    return null;
//		}
//		
//		CipherParamRequest cipher = new CipherParamRequest();
//		cipher.setAlgorithm(Constants.ALGORITHM_AES256);
//		cipher.setCharset(Constants.CHARSET_UTF8);
//		
//		Crypto crypto = new Crypto();
//		ArrayList<String> datalist = new ArrayList<String>();
//		datalist.add(text);
//		
//		crypto.setSecurityKey(key);
//		crypto.setDataList(datalist);
//		crypto.setCipher(cipher);
//		
//		crypto = restTemplate.postForObject(this.url + "/v1/crypto/encrypt", crypto, Crypto.class);
//		
//		return crypto.getDataList().get(0);
//	}
//	
//	public String decrypt(String text, String key) {
//		if (text == null || key == null) {
//		    return null;
//		}
//		
//		CipherParamRequest cipher = new CipherParamRequest();
//		cipher.setAlgorithm(Constants.ALGORITHM_AES256);
//		cipher.setCharset(Constants.CHARSET_UTF8);
//		
//		Crypto crypto = new Crypto();
//		ArrayList<String> datalist = new ArrayList<String>();
//		datalist.add(text);
//		
//		crypto.setSecurityKey(key);
//		crypto.setDataList(datalist);
//		crypto.setCipher(cipher);
//		
//		crypto = restTemplate.postForObject(this.url + "/v1/crypto/decrypt", crypto, Crypto.class);
//		
//		return crypto.getDataList().get(0);
//	}
	
}
