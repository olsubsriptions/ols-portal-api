package pe.olympus.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pe.olympus.model.GenericResponse;
import pe.olympus.model.Register;
import pe.olympus.model.UserDTO;
import pe.olympus.repository.UserRepository;

@Service
public class UserService {
	
	private final Logger logger = LoggerFactory.getLogger(this.getClass());
	
	UserRepository userRepository;
	
	@Autowired
	public UserService(UserRepository userRepository) {
		
		this.userRepository = userRepository;
	}

	public GenericResponse registerUser(Register register) {
		
		GenericResponse genericResponse = userRepository.registerUser(register);
		logger.info("UserService.registerUser : registerUser() is done.");
		return genericResponse;
	}
	
	public GenericResponse registerGoogleUser(Register register) {
		
		GenericResponse genericResponse = userRepository.registerGoogleUser(register);
		logger.info("UserService.registerGoogleUser : registerGoogleUser() is done.");
		return genericResponse;
	}
	
	public GenericResponse validateUser(UserDTO userDTO) {
		
		GenericResponse genericResponse = userRepository.validateUser(userDTO);
		logger.info("UserService.validateUser : validateUser() is done.");
		return genericResponse;
	}
	
	public GenericResponse validateRegister(UserDTO userDTO) {
		
		GenericResponse genericResponse = userRepository.validateRegister(userDTO);
		logger.info("UserService.validateRegister : validateRegister() is done.");
		return genericResponse;
	}
}
