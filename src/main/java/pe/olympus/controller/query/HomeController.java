package pe.olympus.controller.query;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/")
public class HomeController {
	
	@GetMapping
	@ResponseStatus(HttpStatus.OK)
	public String home() {
		return "Ols Portal Api STARTED";
	}	
}
