package pe.olympus.controller.query;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import pe.olympus.constants.Countries;

@RestController
@RequestMapping("/countries")
public class CountryControllerQuery {
	
	@GetMapping()
	@ResponseStatus(HttpStatus.OK)
	public ResponseEntity<String> getCountries (){
		
		return new ResponseEntity<String>(Countries.COUNTRIES_CODE, HttpStatus.OK);		
	}

}


