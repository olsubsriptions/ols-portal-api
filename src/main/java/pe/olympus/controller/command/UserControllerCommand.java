package pe.olympus.controller.command;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import pe.olympus.model.GenericResponse;
import pe.olympus.model.Register;
import pe.olympus.model.UserDTO;
import pe.olympus.service.UserService;

@RestController
@RequestMapping("/users")
public class UserControllerCommand {
	
	private UserService userService;
	
	@Autowired
	public UserControllerCommand(UserService userService) {
		this.userService = userService;
		
	}
	
	@PostMapping
	@ResponseStatus(HttpStatus.OK)
	public ResponseEntity<GenericResponse> registerUser (@RequestBody @Valid Register register){
		GenericResponse genericResponse = userService.registerUser(register);
		return new ResponseEntity<GenericResponse>(genericResponse, HttpStatus.OK);
	}
	
	@PostMapping("/google")
	@ResponseStatus(HttpStatus.OK)
	public ResponseEntity<GenericResponse> registerGoogleUser (@RequestBody @Valid Register register){
		GenericResponse genericResponse = userService.registerGoogleUser(register);
		return new ResponseEntity<GenericResponse>(genericResponse, HttpStatus.OK);
	}

	@PostMapping("/validate/user")
	@ResponseStatus(HttpStatus.OK)
	public ResponseEntity<GenericResponse> validateUser(@RequestBody @Valid UserDTO userDTO){	
		
		GenericResponse genericResponse = userService.validateUser(userDTO);			
		return new ResponseEntity<GenericResponse>(genericResponse, HttpStatus.OK);
	}
	
	@PostMapping("/validate/register")
	@ResponseStatus(HttpStatus.OK)
	public ResponseEntity<GenericResponse> validateRegister(@RequestBody @Valid UserDTO userDTO){	
		
		GenericResponse genericResponse = userService.validateRegister(userDTO);			
		return new ResponseEntity<GenericResponse>(genericResponse, HttpStatus.OK);
	}
}
