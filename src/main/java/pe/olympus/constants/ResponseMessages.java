package pe.olympus.constants;

public class ResponseMessages {
	public static final String OK = "La operación se ha realizado exitosamente";
	public static final String OPERACION_EXITOSA = "La operación se ha realizado exitosamente";
	public static final String OPERACION_NOEXITOSA = "La operación no se ha completado";
	public static final String SUSCRIPCION_VALIDA = "Suscripción Válida";
	public static final String SUSCRIPCION_INVALIDA = "Suscripción Inválida";
	public static final String APLICACION_NOEXISTE = "La Aplicación no existe";
	public static final String SUSCRIPCION_EXISTE = "Ya cuenta con una Suscripción para esta Aplicación";
	public static final String CIPHER_STRUCTURE_ERROR = "La estructura del cipher no es correcta";
	public static final String SUSCRIPCION_DUPLICADA = "La Suscripción ya existe";
	public static final String USUARIO_DUPLICADO = "El Usuario ya existe";
	public static final String ORGANIZACION_DUPLICADA = "La Organización ya existe";
	public static final String DATABASE_ERROR = "Error en base de datos";
}
