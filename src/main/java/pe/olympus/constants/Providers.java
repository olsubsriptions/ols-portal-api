package pe.olympus.constants;

public class Providers {

	public static final String MYSQL =  "mysqlDataProvider";
	public static final String SQL =  "sqlDataProvider";
	public static final String POSTGRESQL =  "postgresqlDataProvider";
	public static final String ORACLE =  "oracleDataProvider";
}
