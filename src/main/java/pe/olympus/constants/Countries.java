package pe.olympus.constants;

public class Countries {
	
	public static final String COUNTRIES_CODE = "[\r\n" + 
			"    {\r\n" + 
			"        \"name\": \"Peru\",\r\n" + 
			"        \"prefix\": \"+51\",\r\n" + 
			"        \"code\": \"PE \"\r\n" + 
			"    },\r\n" + 
			"    {\r\n" + 
			"        \"name\": \"Afghanistan\",\r\n" + 
			"        \"prefix\": \"+93\",\r\n" + 
			"        \"code\": \"AF \"\r\n" + 
			"    },\r\n" + 
			"    {\r\n" + 
			"        \"name\": \"Albania\",\r\n" + 
			"        \"prefix\": \"+355\",\r\n" + 
			"        \"code\": \"AL \"\r\n" + 
			"    },\r\n" + 
			"    {\r\n" + 
			"        \"name\": \"Algeria\",\r\n" + 
			"        \"prefix\": \"+213\",\r\n" + 
			"        \"code\": \"DZ \"\r\n" + 
			"    },\r\n" + 
			"    {\r\n" + 
			"        \"name\": \"American Samoa\",\r\n" + 
			"        \"prefix\": \"+1-684\",\r\n" + 
			"        \"code\": \"AS \"\r\n" + 
			"    },\r\n" + 
			"    {\r\n" + 
			"        \"name\": \"Andorra\",\r\n" + 
			"        \"prefix\": \"+376\",\r\n" + 
			"        \"code\": \"AD \"\r\n" + 
			"    },\r\n" + 
			"    {\r\n" + 
			"        \"name\": \"Angola\",\r\n" + 
			"        \"prefix\": \"+244\",\r\n" + 
			"        \"code\": \"AO \"\r\n" + 
			"    },\r\n" + 
			"    {\r\n" + 
			"        \"name\": \"Anguilla\",\r\n" + 
			"        \"prefix\": \"+1-264\",\r\n" + 
			"        \"code\": \"AI \"\r\n" + 
			"    },\r\n" + 
			"    {\r\n" + 
			"        \"name\": \"Antarctica\",\r\n" + 
			"        \"prefix\": \"+672\",\r\n" + 
			"        \"code\": \"AQ \"\r\n" + 
			"    },\r\n" + 
			"    {\r\n" + 
			"        \"name\": \"Antigua and Barbuda\",\r\n" + 
			"        \"prefix\": \"+1-268\",\r\n" + 
			"        \"code\": \"AG \"\r\n" + 
			"    },\r\n" + 
			"    {\r\n" + 
			"        \"name\": \"Argentina\",\r\n" + 
			"        \"prefix\": \"+54\",\r\n" + 
			"        \"code\": \"AR \"\r\n" + 
			"    },\r\n" + 
			"    {\r\n" + 
			"        \"name\": \"Armenia\",\r\n" + 
			"        \"prefix\": \"+374\",\r\n" + 
			"        \"code\": \"AM \"\r\n" + 
			"    },\r\n" + 
			"    {\r\n" + 
			"        \"name\": \"Aruba\",\r\n" + 
			"        \"prefix\": \"+297\",\r\n" + 
			"        \"code\": \"AW \"\r\n" + 
			"    },\r\n" + 
			"    {\r\n" + 
			"        \"name\": \"Australia\",\r\n" + 
			"        \"prefix\": \"+61\",\r\n" + 
			"        \"code\": \"AU \"\r\n" + 
			"    },\r\n" + 
			"    {\r\n" + 
			"        \"name\": \"Austria\",\r\n" + 
			"        \"prefix\": \"+43\",\r\n" + 
			"        \"code\": \"AT \"\r\n" + 
			"    },\r\n" + 
			"    {\r\n" + 
			"        \"name\": \"Azerbaijan\",\r\n" + 
			"        \"prefix\": \"+994\",\r\n" + 
			"        \"code\": \"AZ \"\r\n" + 
			"    },\r\n" + 
			"    {\r\n" + 
			"        \"name\": \"Bahamas\",\r\n" + 
			"        \"prefix\": \"+1-242\",\r\n" + 
			"        \"code\": \"BS \"\r\n" + 
			"    },\r\n" + 
			"    {\r\n" + 
			"        \"name\": \"Bahrain\",\r\n" + 
			"        \"prefix\": \"+973\",\r\n" + 
			"        \"code\": \"BH \"\r\n" + 
			"    },\r\n" + 
			"    {\r\n" + 
			"        \"name\": \"Bangladesh\",\r\n" + 
			"        \"prefix\": \"+880\",\r\n" + 
			"        \"code\": \"BD \"\r\n" + 
			"    },\r\n" + 
			"    {\r\n" + 
			"        \"name\": \"Barbados\",\r\n" + 
			"        \"prefix\": \"+1-246\",\r\n" + 
			"        \"code\": \"BB \"\r\n" + 
			"    },\r\n" + 
			"    {\r\n" + 
			"        \"name\": \"Belarus\",\r\n" + 
			"        \"prefix\": \"+375\",\r\n" + 
			"        \"code\": \"BY \"\r\n" + 
			"    },\r\n" + 
			"    {\r\n" + 
			"        \"name\": \"Belgium\",\r\n" + 
			"        \"prefix\": \"+32\",\r\n" + 
			"        \"code\": \"BE \"\r\n" + 
			"    },\r\n" + 
			"    {\r\n" + 
			"        \"name\": \"Belize\",\r\n" + 
			"        \"prefix\": \"+501\",\r\n" + 
			"        \"code\": \"BZ \"\r\n" + 
			"    },\r\n" + 
			"    {\r\n" + 
			"        \"name\": \"Benin\",\r\n" + 
			"        \"prefix\": \"+229\",\r\n" + 
			"        \"code\": \"BJ \"\r\n" + 
			"    },\r\n" + 
			"    {\r\n" + 
			"        \"name\": \"Bermuda\",\r\n" + 
			"        \"prefix\": \"+1-441\",\r\n" + 
			"        \"code\": \"BM \"\r\n" + 
			"    },\r\n" + 
			"    {\r\n" + 
			"        \"name\": \"Bhutan\",\r\n" + 
			"        \"prefix\": \"+975\",\r\n" + 
			"        \"code\": \"BT \"\r\n" + 
			"    },\r\n" + 
			"    {\r\n" + 
			"        \"name\": \"Bolivia\",\r\n" + 
			"        \"prefix\": \"+591\",\r\n" + 
			"        \"code\": \"BO \"\r\n" + 
			"    },\r\n" + 
			"    {\r\n" + 
			"        \"name\": \"Bosnia and Herzegovina\",\r\n" + 
			"        \"prefix\": \"+387\",\r\n" + 
			"        \"code\": \"BA \"\r\n" + 
			"    },\r\n" + 
			"    {\r\n" + 
			"        \"name\": \"Botswana\",\r\n" + 
			"        \"prefix\": \"+267\",\r\n" + 
			"        \"code\": \"BW \"\r\n" + 
			"    },\r\n" + 
			"    {\r\n" + 
			"        \"name\": \"Brazil\",\r\n" + 
			"        \"prefix\": \"+55\",\r\n" + 
			"        \"code\": \"BR \"\r\n" + 
			"    },\r\n" + 
			"    {\r\n" + 
			"        \"name\": \"British Indian Ocean Territory\",\r\n" + 
			"        \"prefix\": \"+246\",\r\n" + 
			"        \"code\": \"IO \"\r\n" + 
			"    },\r\n" + 
			"    {\r\n" + 
			"        \"name\": \"British Virgin Islands\",\r\n" + 
			"        \"prefix\": \"+1-284\",\r\n" + 
			"        \"code\": \"VG \"\r\n" + 
			"    },\r\n" + 
			"    {\r\n" + 
			"        \"name\": \"Brunei\",\r\n" + 
			"        \"prefix\": \"+673\",\r\n" + 
			"        \"code\": \"BN \"\r\n" + 
			"    },\r\n" + 
			"    {\r\n" + 
			"        \"name\": \"Bulgaria\",\r\n" + 
			"        \"prefix\": \"+359\",\r\n" + 
			"        \"code\": \"BG \"\r\n" + 
			"    },\r\n" + 
			"    {\r\n" + 
			"        \"name\": \"Burkina Faso\",\r\n" + 
			"        \"prefix\": \"+226\",\r\n" + 
			"        \"code\": \"BF \"\r\n" + 
			"    },\r\n" + 
			"    {\r\n" + 
			"        \"name\": \"Myanmar\",\r\n" + 
			"        \"prefix\": \"+95\",\r\n" + 
			"        \"code\": \"MM \"\r\n" + 
			"    },\r\n" + 
			"    {\r\n" + 
			"        \"name\": \"Burundi\",\r\n" + 
			"        \"prefix\": \"+257\",\r\n" + 
			"        \"code\": \"BI \"\r\n" + 
			"    },\r\n" + 
			"    {\r\n" + 
			"        \"name\": \"Cambodia\",\r\n" + 
			"        \"prefix\": \"+855\",\r\n" + 
			"        \"code\": \"KH \"\r\n" + 
			"    },\r\n" + 
			"    {\r\n" + 
			"        \"name\": \"Cameroon\",\r\n" + 
			"        \"prefix\": \"+237\",\r\n" + 
			"        \"code\": \"CM \"\r\n" + 
			"    },\r\n" + 
			"    {\r\n" + 
			"        \"name\": \"Canada\",\r\n" + 
			"        \"prefix\": \"+1\",\r\n" + 
			"        \"code\": \"CA \"\r\n" + 
			"    },\r\n" + 
			"    {\r\n" + 
			"        \"name\": \"Cape Verde\",\r\n" + 
			"        \"prefix\": \"+238\",\r\n" + 
			"        \"code\": \"CV \"\r\n" + 
			"    },\r\n" + 
			"    {\r\n" + 
			"        \"name\": \"Cayman Islands\",\r\n" + 
			"        \"prefix\": \"+1-345\",\r\n" + 
			"        \"code\": \"KY \"\r\n" + 
			"    },\r\n" + 
			"    {\r\n" + 
			"        \"name\": \"Central African Republic\",\r\n" + 
			"        \"prefix\": \"+236\",\r\n" + 
			"        \"code\": \"CF \"\r\n" + 
			"    },\r\n" + 
			"    {\r\n" + 
			"        \"name\": \"Chad\",\r\n" + 
			"        \"prefix\": \"+235\",\r\n" + 
			"        \"code\": \"TD \"\r\n" + 
			"    },\r\n" + 
			"    {\r\n" + 
			"        \"name\": \"Chile\",\r\n" + 
			"        \"prefix\": \"+56\",\r\n" + 
			"        \"code\": \"CL \"\r\n" + 
			"    },\r\n" + 
			"    {\r\n" + 
			"        \"name\": \"China\",\r\n" + 
			"        \"prefix\": \"+86\",\r\n" + 
			"        \"code\": \"CN \"\r\n" + 
			"    },\r\n" + 
			"    {\r\n" + 
			"        \"name\": \"Christmas Island\",\r\n" + 
			"        \"prefix\": \"+61\",\r\n" + 
			"        \"code\": \"CX \"\r\n" + 
			"    },\r\n" + 
			"    {\r\n" + 
			"        \"name\": \"Cocos Islands\",\r\n" + 
			"        \"prefix\": \"+61\",\r\n" + 
			"        \"code\": \"CC \"\r\n" + 
			"    },\r\n" + 
			"    {\r\n" + 
			"        \"name\": \"Colombia\",\r\n" + 
			"        \"prefix\": \"+57\",\r\n" + 
			"        \"code\": \"CO \"\r\n" + 
			"    },\r\n" + 
			"    {\r\n" + 
			"        \"name\": \"Comoros\",\r\n" + 
			"        \"prefix\": \"+269\",\r\n" + 
			"        \"code\": \"KM \"\r\n" + 
			"    },\r\n" + 
			"    {\r\n" + 
			"        \"name\": \"Republic of the Congo\",\r\n" + 
			"        \"prefix\": \"+242\",\r\n" + 
			"        \"code\": \"CG \"\r\n" + 
			"    },\r\n" + 
			"    {\r\n" + 
			"        \"name\": \"Democratic Republic of the Congo\",\r\n" + 
			"        \"prefix\": \"+243\",\r\n" + 
			"        \"code\": \"CD \"\r\n" + 
			"    },\r\n" + 
			"    {\r\n" + 
			"        \"name\": \"Cook Islands\",\r\n" + 
			"        \"prefix\": \"+682\",\r\n" + 
			"        \"code\": \"CK \"\r\n" + 
			"    },\r\n" + 
			"    {\r\n" + 
			"        \"name\": \"Costa Rica\",\r\n" + 
			"        \"prefix\": \"+506\",\r\n" + 
			"        \"code\": \"CR \"\r\n" + 
			"    },\r\n" + 
			"    {\r\n" + 
			"        \"name\": \"Croatia\",\r\n" + 
			"        \"prefix\": \"+385\",\r\n" + 
			"        \"code\": \"HR \"\r\n" + 
			"    },\r\n" + 
			"    {\r\n" + 
			"        \"name\": \"Cuba\",\r\n" + 
			"        \"prefix\": \"+53\",\r\n" + 
			"        \"code\": \"CU \"\r\n" + 
			"    },\r\n" + 
			"    {\r\n" + 
			"        \"name\": \"Curacao\",\r\n" + 
			"        \"prefix\": \"+599\",\r\n" + 
			"        \"code\": \"CW \"\r\n" + 
			"    },\r\n" + 
			"    {\r\n" + 
			"        \"name\": \"Cyprus\",\r\n" + 
			"        \"prefix\": \"+357\",\r\n" + 
			"        \"code\": \"CY \"\r\n" + 
			"    },\r\n" + 
			"    {\r\n" + 
			"        \"name\": \"Czech Republic\",\r\n" + 
			"        \"prefix\": \"+420\",\r\n" + 
			"        \"code\": \"CZ \"\r\n" + 
			"    },\r\n" + 
			"    {\r\n" + 
			"        \"name\": \"Denmark\",\r\n" + 
			"        \"prefix\": \"+45\",\r\n" + 
			"        \"code\": \"DK \"\r\n" + 
			"    },\r\n" + 
			"    {\r\n" + 
			"        \"name\": \"Djibouti\",\r\n" + 
			"        \"prefix\": \"+253\",\r\n" + 
			"        \"code\": \"DJ \"\r\n" + 
			"    },\r\n" + 
			"    {\r\n" + 
			"        \"name\": \"Dominica\",\r\n" + 
			"        \"prefix\": \"+1-767\",\r\n" + 
			"        \"code\": \"DM \"\r\n" + 
			"    },\r\n" + 
			"    {\r\n" + 
			"        \"name\": \"Dominican Republic\",\r\n" + 
			"        \"prefix\": \"+1-809, 1-829, 1-849\",\r\n" + 
			"        \"code\": \"DO \"\r\n" + 
			"    },\r\n" + 
			"    {\r\n" + 
			"        \"name\": \"East Timor\",\r\n" + 
			"        \"prefix\": \"+670\",\r\n" + 
			"        \"code\": \"TL \"\r\n" + 
			"    },\r\n" + 
			"    {\r\n" + 
			"        \"name\": \"Ecuador\",\r\n" + 
			"        \"prefix\": \"+593\",\r\n" + 
			"        \"code\": \"EC \"\r\n" + 
			"    },\r\n" + 
			"    {\r\n" + 
			"        \"name\": \"Egypt\",\r\n" + 
			"        \"prefix\": \"+20\",\r\n" + 
			"        \"code\": \"EG \"\r\n" + 
			"    },\r\n" + 
			"    {\r\n" + 
			"        \"name\": \"El Salvador\",\r\n" + 
			"        \"prefix\": \"+503\",\r\n" + 
			"        \"code\": \"SV \"\r\n" + 
			"    },\r\n" + 
			"    {\r\n" + 
			"        \"name\": \"Equatorial Guinea\",\r\n" + 
			"        \"prefix\": \"+240\",\r\n" + 
			"        \"code\": \"GQ \"\r\n" + 
			"    },\r\n" + 
			"    {\r\n" + 
			"        \"name\": \"Eritrea\",\r\n" + 
			"        \"prefix\": \"+291\",\r\n" + 
			"        \"code\": \"ER \"\r\n" + 
			"    },\r\n" + 
			"    {\r\n" + 
			"        \"name\": \"Estonia\",\r\n" + 
			"        \"prefix\": \"+372\",\r\n" + 
			"        \"code\": \"EE \"\r\n" + 
			"    },\r\n" + 
			"    {\r\n" + 
			"        \"name\": \"Ethiopia\",\r\n" + 
			"        \"prefix\": \"+251\",\r\n" + 
			"        \"code\": \"ET \"\r\n" + 
			"    },\r\n" + 
			"    {\r\n" + 
			"        \"name\": \"Falkland Islands\",\r\n" + 
			"        \"prefix\": \"+500\",\r\n" + 
			"        \"code\": \"FK \"\r\n" + 
			"    },\r\n" + 
			"    {\r\n" + 
			"        \"name\": \"Faroe Islands\",\r\n" + 
			"        \"prefix\": \"+298\",\r\n" + 
			"        \"code\": \"FO \"\r\n" + 
			"    },\r\n" + 
			"    {\r\n" + 
			"        \"name\": \"Fiji\",\r\n" + 
			"        \"prefix\": \"+679\",\r\n" + 
			"        \"code\": \"FJ \"\r\n" + 
			"    },\r\n" + 
			"    {\r\n" + 
			"        \"name\": \"Finland\",\r\n" + 
			"        \"prefix\": \"+358\",\r\n" + 
			"        \"code\": \"FI \"\r\n" + 
			"    },\r\n" + 
			"    {\r\n" + 
			"        \"name\": \"France\",\r\n" + 
			"        \"prefix\": \"+33\",\r\n" + 
			"        \"code\": \"FR \"\r\n" + 
			"    },\r\n" + 
			"    {\r\n" + 
			"        \"name\": \"French Polynesia\",\r\n" + 
			"        \"prefix\": \"+689\",\r\n" + 
			"        \"code\": \"PF \"\r\n" + 
			"    },\r\n" + 
			"    {\r\n" + 
			"        \"name\": \"Gabon\",\r\n" + 
			"        \"prefix\": \"+241\",\r\n" + 
			"        \"code\": \"GA \"\r\n" + 
			"    },\r\n" + 
			"    {\r\n" + 
			"        \"name\": \"Gambia\",\r\n" + 
			"        \"prefix\": \"+220\",\r\n" + 
			"        \"code\": \"GM \"\r\n" + 
			"    },\r\n" + 
			"    {\r\n" + 
			"        \"name\": \"Georgia\",\r\n" + 
			"        \"prefix\": \"+995\",\r\n" + 
			"        \"code\": \"GE \"\r\n" + 
			"    },\r\n" + 
			"    {\r\n" + 
			"        \"name\": \"Germany\",\r\n" + 
			"        \"prefix\": \"+49\",\r\n" + 
			"        \"code\": \"DE \"\r\n" + 
			"    },\r\n" + 
			"    {\r\n" + 
			"        \"name\": \"Ghana\",\r\n" + 
			"        \"prefix\": \"+233\",\r\n" + 
			"        \"code\": \"GH \"\r\n" + 
			"    },\r\n" + 
			"    {\r\n" + 
			"        \"name\": \"Gibraltar\",\r\n" + 
			"        \"prefix\": \"+350\",\r\n" + 
			"        \"code\": \"GI \"\r\n" + 
			"    },\r\n" + 
			"    {\r\n" + 
			"        \"name\": \"Greece\",\r\n" + 
			"        \"prefix\": \"+30\",\r\n" + 
			"        \"code\": \"GR \"\r\n" + 
			"    },\r\n" + 
			"    {\r\n" + 
			"        \"name\": \"Greenland\",\r\n" + 
			"        \"prefix\": \"+299\",\r\n" + 
			"        \"code\": \"GL \"\r\n" + 
			"    },\r\n" + 
			"    {\r\n" + 
			"        \"name\": \"Grenada\",\r\n" + 
			"        \"prefix\": \"+1-473\",\r\n" + 
			"        \"code\": \"GD \"\r\n" + 
			"    },\r\n" + 
			"    {\r\n" + 
			"        \"name\": \"Guam\",\r\n" + 
			"        \"prefix\": \"+1-671\",\r\n" + 
			"        \"code\": \"GU \"\r\n" + 
			"    },\r\n" + 
			"    {\r\n" + 
			"        \"name\": \"Guatemala\",\r\n" + 
			"        \"prefix\": \"+502\",\r\n" + 
			"        \"code\": \"GT \"\r\n" + 
			"    },\r\n" + 
			"    {\r\n" + 
			"        \"name\": \"Guernsey\",\r\n" + 
			"        \"prefix\": \"+44-1481\",\r\n" + 
			"        \"code\": \"GG \"\r\n" + 
			"    },\r\n" + 
			"    {\r\n" + 
			"        \"name\": \"Guinea\",\r\n" + 
			"        \"prefix\": \"+224\",\r\n" + 
			"        \"code\": \"GN \"\r\n" + 
			"    },\r\n" + 
			"    {\r\n" + 
			"        \"name\": \"Guinea-Bissau\",\r\n" + 
			"        \"prefix\": \"+245\",\r\n" + 
			"        \"code\": \"GW \"\r\n" + 
			"    },\r\n" + 
			"    {\r\n" + 
			"        \"name\": \"Guyana\",\r\n" + 
			"        \"prefix\": \"+592\",\r\n" + 
			"        \"code\": \"GY \"\r\n" + 
			"    },\r\n" + 
			"    {\r\n" + 
			"        \"name\": \"Haiti\",\r\n" + 
			"        \"prefix\": \"+509\",\r\n" + 
			"        \"code\": \"HT \"\r\n" + 
			"    },\r\n" + 
			"    {\r\n" + 
			"        \"name\": \"Honduras\",\r\n" + 
			"        \"prefix\": \"+504\",\r\n" + 
			"        \"code\": \"HN \"\r\n" + 
			"    },\r\n" + 
			"    {\r\n" + 
			"        \"name\": \"Hong Kong\",\r\n" + 
			"        \"prefix\": \"+852\",\r\n" + 
			"        \"code\": \"HK \"\r\n" + 
			"    },\r\n" + 
			"    {\r\n" + 
			"        \"name\": \"Hungary\",\r\n" + 
			"        \"prefix\": \"+36\",\r\n" + 
			"        \"code\": \"HU \"\r\n" + 
			"    },\r\n" + 
			"    {\r\n" + 
			"        \"name\": \"Iceland\",\r\n" + 
			"        \"prefix\": \"+354\",\r\n" + 
			"        \"code\": \"IS \"\r\n" + 
			"    },\r\n" + 
			"    {\r\n" + 
			"        \"name\": \"India\",\r\n" + 
			"        \"prefix\": \"+91\",\r\n" + 
			"        \"code\": \"IN \"\r\n" + 
			"    },\r\n" + 
			"    {\r\n" + 
			"        \"name\": \"Indonesia\",\r\n" + 
			"        \"prefix\": \"+62\",\r\n" + 
			"        \"code\": \"ID \"\r\n" + 
			"    },\r\n" + 
			"    {\r\n" + 
			"        \"name\": \"Iran\",\r\n" + 
			"        \"prefix\": \"+98\",\r\n" + 
			"        \"code\": \"IR \"\r\n" + 
			"    },\r\n" + 
			"    {\r\n" + 
			"        \"name\": \"Iraq\",\r\n" + 
			"        \"prefix\": \"+964\",\r\n" + 
			"        \"code\": \"IQ \"\r\n" + 
			"    },\r\n" + 
			"    {\r\n" + 
			"        \"name\": \"Ireland\",\r\n" + 
			"        \"prefix\": \"+353\",\r\n" + 
			"        \"code\": \"IE \"\r\n" + 
			"    },\r\n" + 
			"    {\r\n" + 
			"        \"name\": \"Isle of Man\",\r\n" + 
			"        \"prefix\": \"+44-1624\",\r\n" + 
			"        \"code\": \"IM \"\r\n" + 
			"    },\r\n" + 
			"    {\r\n" + 
			"        \"name\": \"Israel\",\r\n" + 
			"        \"prefix\": \"+972\",\r\n" + 
			"        \"code\": \"IL \"\r\n" + 
			"    },\r\n" + 
			"    {\r\n" + 
			"        \"name\": \"Italy\",\r\n" + 
			"        \"prefix\": \"+39\",\r\n" + 
			"        \"code\": \"IT \"\r\n" + 
			"    },\r\n" + 
			"    {\r\n" + 
			"        \"name\": \"Ivory Coast\",\r\n" + 
			"        \"prefix\": \"+225\",\r\n" + 
			"        \"code\": \"CI \"\r\n" + 
			"    },\r\n" + 
			"    {\r\n" + 
			"        \"name\": \"Jamaica\",\r\n" + 
			"        \"prefix\": \"+1-876\",\r\n" + 
			"        \"code\": \"JM \"\r\n" + 
			"    },\r\n" + 
			"    {\r\n" + 
			"        \"name\": \"Japan\",\r\n" + 
			"        \"prefix\": \"+81\",\r\n" + 
			"        \"code\": \"JP \"\r\n" + 
			"    },\r\n" + 
			"    {\r\n" + 
			"        \"name\": \"Jersey\",\r\n" + 
			"        \"prefix\": \"+44-1534\",\r\n" + 
			"        \"code\": \"JE \"\r\n" + 
			"    },\r\n" + 
			"    {\r\n" + 
			"        \"name\": \"Jordan\",\r\n" + 
			"        \"prefix\": \"+962\",\r\n" + 
			"        \"code\": \"JO \"\r\n" + 
			"    },\r\n" + 
			"    {\r\n" + 
			"        \"name\": \"Kazakhstan\",\r\n" + 
			"        \"prefix\": \"+7\",\r\n" + 
			"        \"code\": \"KZ \"\r\n" + 
			"    },\r\n" + 
			"    {\r\n" + 
			"        \"name\": \"Kenya\",\r\n" + 
			"        \"prefix\": \"+254\",\r\n" + 
			"        \"code\": \"KE \"\r\n" + 
			"    },\r\n" + 
			"    {\r\n" + 
			"        \"name\": \"Kiribati\",\r\n" + 
			"        \"prefix\": \"+686\",\r\n" + 
			"        \"code\": \"KI \"\r\n" + 
			"    },\r\n" + 
			"    {\r\n" + 
			"        \"name\": \"Kosovo\",\r\n" + 
			"        \"prefix\": \"+383\",\r\n" + 
			"        \"code\": \"XK \"\r\n" + 
			"    },\r\n" + 
			"    {\r\n" + 
			"        \"name\": \"Kuwait\",\r\n" + 
			"        \"prefix\": \"+965\",\r\n" + 
			"        \"code\": \"KW \"\r\n" + 
			"    },\r\n" + 
			"    {\r\n" + 
			"        \"name\": \"Kyrgyzstan\",\r\n" + 
			"        \"prefix\": \"+996\",\r\n" + 
			"        \"code\": \"KG \"\r\n" + 
			"    },\r\n" + 
			"    {\r\n" + 
			"        \"name\": \"Laos\",\r\n" + 
			"        \"prefix\": \"+856\",\r\n" + 
			"        \"code\": \"LA \"\r\n" + 
			"    },\r\n" + 
			"    {\r\n" + 
			"        \"name\": \"Latvia\",\r\n" + 
			"        \"prefix\": \"+371\",\r\n" + 
			"        \"code\": \"LV \"\r\n" + 
			"    },\r\n" + 
			"    {\r\n" + 
			"        \"name\": \"Lebanon\",\r\n" + 
			"        \"prefix\": \"+961\",\r\n" + 
			"        \"code\": \"LB \"\r\n" + 
			"    },\r\n" + 
			"    {\r\n" + 
			"        \"name\": \"Lesotho\",\r\n" + 
			"        \"prefix\": \"+266\",\r\n" + 
			"        \"code\": \"LS \"\r\n" + 
			"    },\r\n" + 
			"    {\r\n" + 
			"        \"name\": \"Liberia\",\r\n" + 
			"        \"prefix\": \"+231\",\r\n" + 
			"        \"code\": \"LR \"\r\n" + 
			"    },\r\n" + 
			"    {\r\n" + 
			"        \"name\": \"Libya\",\r\n" + 
			"        \"prefix\": \"+218\",\r\n" + 
			"        \"code\": \"LY \"\r\n" + 
			"    },\r\n" + 
			"    {\r\n" + 
			"        \"name\": \"Liechtenstein\",\r\n" + 
			"        \"prefix\": \"+423\",\r\n" + 
			"        \"code\": \"LI \"\r\n" + 
			"    },\r\n" + 
			"    {\r\n" + 
			"        \"name\": \"Lithuania\",\r\n" + 
			"        \"prefix\": \"+370\",\r\n" + 
			"        \"code\": \"LT \"\r\n" + 
			"    },\r\n" + 
			"    {\r\n" + 
			"        \"name\": \"Luxembourg\",\r\n" + 
			"        \"prefix\": \"+352\",\r\n" + 
			"        \"code\": \"LU \"\r\n" + 
			"    },\r\n" + 
			"    {\r\n" + 
			"        \"name\": \"Macau\",\r\n" + 
			"        \"prefix\": \"+853\",\r\n" + 
			"        \"code\": \"MO \"\r\n" + 
			"    },\r\n" + 
			"    {\r\n" + 
			"        \"name\": \"Macedonia\",\r\n" + 
			"        \"prefix\": \"+389\",\r\n" + 
			"        \"code\": \"MK \"\r\n" + 
			"    },\r\n" + 
			"    {\r\n" + 
			"        \"name\": \"Madagascar\",\r\n" + 
			"        \"prefix\": \"+261\",\r\n" + 
			"        \"code\": \"MG \"\r\n" + 
			"    },\r\n" + 
			"    {\r\n" + 
			"        \"name\": \"Malawi\",\r\n" + 
			"        \"prefix\": \"+265\",\r\n" + 
			"        \"code\": \"MW \"\r\n" + 
			"    },\r\n" + 
			"    {\r\n" + 
			"        \"name\": \"Malaysia\",\r\n" + 
			"        \"prefix\": \"+60\",\r\n" + 
			"        \"code\": \"MY \"\r\n" + 
			"    },\r\n" + 
			"    {\r\n" + 
			"        \"name\": \"Maldives\",\r\n" + 
			"        \"prefix\": \"+960\",\r\n" + 
			"        \"code\": \"MV \"\r\n" + 
			"    },\r\n" + 
			"    {\r\n" + 
			"        \"name\": \"Mali\",\r\n" + 
			"        \"prefix\": \"+223\",\r\n" + 
			"        \"code\": \"ML \"\r\n" + 
			"    },\r\n" + 
			"    {\r\n" + 
			"        \"name\": \"Malta\",\r\n" + 
			"        \"prefix\": \"+356\",\r\n" + 
			"        \"code\": \"MT \"\r\n" + 
			"    },\r\n" + 
			"    {\r\n" + 
			"        \"name\": \"Marshall Islands\",\r\n" + 
			"        \"prefix\": \"+692\",\r\n" + 
			"        \"code\": \"MH \"\r\n" + 
			"    },\r\n" + 
			"    {\r\n" + 
			"        \"name\": \"Mauritania\",\r\n" + 
			"        \"prefix\": \"+222\",\r\n" + 
			"        \"code\": \"MR \"\r\n" + 
			"    },\r\n" + 
			"    {\r\n" + 
			"        \"name\": \"Mauritius\",\r\n" + 
			"        \"prefix\": \"+230\",\r\n" + 
			"        \"code\": \"MU \"\r\n" + 
			"    },\r\n" + 
			"    {\r\n" + 
			"        \"name\": \"Mayotte\",\r\n" + 
			"        \"prefix\": \"+262\",\r\n" + 
			"        \"code\": \"YT \"\r\n" + 
			"    },\r\n" + 
			"    {\r\n" + 
			"        \"name\": \"Mexico\",\r\n" + 
			"        \"prefix\": \"+52\",\r\n" + 
			"        \"code\": \"MX \"\r\n" + 
			"    },\r\n" + 
			"    {\r\n" + 
			"        \"name\": \"Micronesia\",\r\n" + 
			"        \"prefix\": \"+691\",\r\n" + 
			"        \"code\": \"FM \"\r\n" + 
			"    },\r\n" + 
			"    {\r\n" + 
			"        \"name\": \"Moldova\",\r\n" + 
			"        \"prefix\": \"+373\",\r\n" + 
			"        \"code\": \"MD \"\r\n" + 
			"    },\r\n" + 
			"    {\r\n" + 
			"        \"name\": \"Monaco\",\r\n" + 
			"        \"prefix\": \"+377\",\r\n" + 
			"        \"code\": \"MC \"\r\n" + 
			"    },\r\n" + 
			"    {\r\n" + 
			"        \"name\": \"Mongolia\",\r\n" + 
			"        \"prefix\": \"+976\",\r\n" + 
			"        \"code\": \"MN \"\r\n" + 
			"    },\r\n" + 
			"    {\r\n" + 
			"        \"name\": \"Montenegro\",\r\n" + 
			"        \"prefix\": \"+382\",\r\n" + 
			"        \"code\": \"ME \"\r\n" + 
			"    },\r\n" + 
			"    {\r\n" + 
			"        \"name\": \"Montserrat\",\r\n" + 
			"        \"prefix\": \"+1-664\",\r\n" + 
			"        \"code\": \"MS \"\r\n" + 
			"    },\r\n" + 
			"    {\r\n" + 
			"        \"name\": \"Morocco\",\r\n" + 
			"        \"prefix\": \"+212\",\r\n" + 
			"        \"code\": \"MA \"\r\n" + 
			"    },\r\n" + 
			"    {\r\n" + 
			"        \"name\": \"Mozambique\",\r\n" + 
			"        \"prefix\": \"+258\",\r\n" + 
			"        \"code\": \"MZ \"\r\n" + 
			"    },\r\n" + 
			"    {\r\n" + 
			"        \"name\": \"Namibia\",\r\n" + 
			"        \"prefix\": \"+264\",\r\n" + 
			"        \"code\": \"NA \"\r\n" + 
			"    },\r\n" + 
			"    {\r\n" + 
			"        \"name\": \"Nauru\",\r\n" + 
			"        \"prefix\": \"+674\",\r\n" + 
			"        \"code\": \"NR \"\r\n" + 
			"    },\r\n" + 
			"    {\r\n" + 
			"        \"name\": \"Nepal\",\r\n" + 
			"        \"prefix\": \"+977\",\r\n" + 
			"        \"code\": \"NP \"\r\n" + 
			"    },\r\n" + 
			"    {\r\n" + 
			"        \"name\": \"Netherlands\",\r\n" + 
			"        \"prefix\": \"+31\",\r\n" + 
			"        \"code\": \"NL \"\r\n" + 
			"    },\r\n" + 
			"    {\r\n" + 
			"        \"name\": \"Netherlands Antilles\",\r\n" + 
			"        \"prefix\": \"+599\",\r\n" + 
			"        \"code\": \"AN \"\r\n" + 
			"    },\r\n" + 
			"    {\r\n" + 
			"        \"name\": \"New Caledonia\",\r\n" + 
			"        \"prefix\": \"+687\",\r\n" + 
			"        \"code\": \"NC \"\r\n" + 
			"    },\r\n" + 
			"    {\r\n" + 
			"        \"name\": \"New Zealand\",\r\n" + 
			"        \"prefix\": \"+64\",\r\n" + 
			"        \"code\": \"NZ \"\r\n" + 
			"    },\r\n" + 
			"    {\r\n" + 
			"        \"name\": \"Nicaragua\",\r\n" + 
			"        \"prefix\": \"+505\",\r\n" + 
			"        \"code\": \"NI \"\r\n" + 
			"    },\r\n" + 
			"    {\r\n" + 
			"        \"name\": \"Niger\",\r\n" + 
			"        \"prefix\": \"+227\",\r\n" + 
			"        \"code\": \"NE \"\r\n" + 
			"    },\r\n" + 
			"    {\r\n" + 
			"        \"name\": \"Nigeria\",\r\n" + 
			"        \"prefix\": \"+234\",\r\n" + 
			"        \"code\": \"NG \"\r\n" + 
			"    },\r\n" + 
			"    {\r\n" + 
			"        \"name\": \"Niue\",\r\n" + 
			"        \"prefix\": \"+683\",\r\n" + 
			"        \"code\": \"NU \"\r\n" + 
			"    },\r\n" + 
			"    {\r\n" + 
			"        \"name\": \"Northern Mariana Islands\",\r\n" + 
			"        \"prefix\": \"+1-670\",\r\n" + 
			"        \"code\": \"MP \"\r\n" + 
			"    },\r\n" + 
			"    {\r\n" + 
			"        \"name\": \"North Korea\",\r\n" + 
			"        \"prefix\": \"+850\",\r\n" + 
			"        \"code\": \"KP \"\r\n" + 
			"    },\r\n" + 
			"    {\r\n" + 
			"        \"name\": \"Norway\",\r\n" + 
			"        \"prefix\": \"+47\",\r\n" + 
			"        \"code\": \"NO \"\r\n" + 
			"    },\r\n" + 
			"    {\r\n" + 
			"        \"name\": \"Oman\",\r\n" + 
			"        \"prefix\": \"+968\",\r\n" + 
			"        \"code\": \"OM \"\r\n" + 
			"    },\r\n" + 
			"    {\r\n" + 
			"        \"name\": \"Pakistan\",\r\n" + 
			"        \"prefix\": \"+92\",\r\n" + 
			"        \"code\": \"PK \"\r\n" + 
			"    },\r\n" + 
			"    {\r\n" + 
			"        \"name\": \"Palau\",\r\n" + 
			"        \"prefix\": \"+680\",\r\n" + 
			"        \"code\": \"PW \"\r\n" + 
			"    },\r\n" + 
			"    {\r\n" + 
			"        \"name\": \"Palestine\",\r\n" + 
			"        \"prefix\": \"+970\",\r\n" + 
			"        \"code\": \"PS \"\r\n" + 
			"    },\r\n" + 
			"    {\r\n" + 
			"        \"name\": \"Panama\",\r\n" + 
			"        \"prefix\": \"+507\",\r\n" + 
			"        \"code\": \"PA \"\r\n" + 
			"    },\r\n" + 
			"    {\r\n" + 
			"        \"name\": \"Papua New Guinea\",\r\n" + 
			"        \"prefix\": \"+675\",\r\n" + 
			"        \"code\": \"PG \"\r\n" + 
			"    },\r\n" + 
			"    {\r\n" + 
			"        \"name\": \"Paraguay\",\r\n" + 
			"        \"prefix\": \"+595\",\r\n" + 
			"        \"code\": \"PY \"\r\n" + 
			"    },\r\n" + 
			"    {\r\n" + 
			"        \"name\": \"Philippines\",\r\n" + 
			"        \"prefix\": \"+63\",\r\n" + 
			"        \"code\": \"PH \"\r\n" + 
			"    },\r\n" + 
			"    {\r\n" + 
			"        \"name\": \"Pitcairn\",\r\n" + 
			"        \"prefix\": \"+64\",\r\n" + 
			"        \"code\": \"PN \"\r\n" + 
			"    },\r\n" + 
			"    {\r\n" + 
			"        \"name\": \"Poland\",\r\n" + 
			"        \"prefix\": \"+48\",\r\n" + 
			"        \"code\": \"PL \"\r\n" + 
			"    },\r\n" + 
			"    {\r\n" + 
			"        \"name\": \"Portugal\",\r\n" + 
			"        \"prefix\": \"+351\",\r\n" + 
			"        \"code\": \"PT \"\r\n" + 
			"    },\r\n" + 
			"    {\r\n" + 
			"        \"name\": \"Puerto Rico\",\r\n" + 
			"        \"prefix\": \"+1-787, 1-939\",\r\n" + 
			"        \"code\": \"PR \"\r\n" + 
			"    },\r\n" + 
			"    {\r\n" + 
			"        \"name\": \"Qatar\",\r\n" + 
			"        \"prefix\": \"+974\",\r\n" + 
			"        \"code\": \"QA \"\r\n" + 
			"    },\r\n" + 
			"    {\r\n" + 
			"        \"name\": \"Reunion\",\r\n" + 
			"        \"prefix\": \"+262\",\r\n" + 
			"        \"code\": \"RE \"\r\n" + 
			"    },\r\n" + 
			"    {\r\n" + 
			"        \"name\": \"Romania\",\r\n" + 
			"        \"prefix\": \"+40\",\r\n" + 
			"        \"code\": \"RO \"\r\n" + 
			"    },\r\n" + 
			"    {\r\n" + 
			"        \"name\": \"Russia\",\r\n" + 
			"        \"prefix\": \"+7\",\r\n" + 
			"        \"code\": \"RU \"\r\n" + 
			"    },\r\n" + 
			"    {\r\n" + 
			"        \"name\": \"Rwanda\",\r\n" + 
			"        \"prefix\": \"+250\",\r\n" + 
			"        \"code\": \"RW \"\r\n" + 
			"    },\r\n" + 
			"    {\r\n" + 
			"        \"name\": \"Saint Barthelemy\",\r\n" + 
			"        \"prefix\": \"+590\",\r\n" + 
			"        \"code\": \"BL \"\r\n" + 
			"    },\r\n" + 
			"    {\r\n" + 
			"        \"name\": \"Samoa\",\r\n" + 
			"        \"prefix\": \"+685\",\r\n" + 
			"        \"code\": \"WS \"\r\n" + 
			"    },\r\n" + 
			"    {\r\n" + 
			"        \"name\": \"San Marino\",\r\n" + 
			"        \"prefix\": \"+378\",\r\n" + 
			"        \"code\": \"SM \"\r\n" + 
			"    },\r\n" + 
			"    {\r\n" + 
			"        \"name\": \"Sao Tome and Principe\",\r\n" + 
			"        \"prefix\": \"+239\",\r\n" + 
			"        \"code\": \"ST \"\r\n" + 
			"    },\r\n" + 
			"    {\r\n" + 
			"        \"name\": \"Saudi Arabia\",\r\n" + 
			"        \"prefix\": \"+966\",\r\n" + 
			"        \"code\": \"SA \"\r\n" + 
			"    },\r\n" + 
			"    {\r\n" + 
			"        \"name\": \"Senegal\",\r\n" + 
			"        \"prefix\": \"+221\",\r\n" + 
			"        \"code\": \"SN \"\r\n" + 
			"    },\r\n" + 
			"    {\r\n" + 
			"        \"name\": \"Serbia\",\r\n" + 
			"        \"prefix\": \"+381\",\r\n" + 
			"        \"code\": \"RS \"\r\n" + 
			"    },\r\n" + 
			"    {\r\n" + 
			"        \"name\": \"Seychelles\",\r\n" + 
			"        \"prefix\": \"+248\",\r\n" + 
			"        \"code\": \"SC \"\r\n" + 
			"    },\r\n" + 
			"    {\r\n" + 
			"        \"name\": \"Sierra Leone\",\r\n" + 
			"        \"prefix\": \"+232\",\r\n" + 
			"        \"code\": \"SL \"\r\n" + 
			"    },\r\n" + 
			"    {\r\n" + 
			"        \"name\": \"Singapore\",\r\n" + 
			"        \"prefix\": \"+65\",\r\n" + 
			"        \"code\": \"SG \"\r\n" + 
			"    },\r\n" + 
			"    {\r\n" + 
			"        \"name\": \"Sint Maarten\",\r\n" + 
			"        \"prefix\": \"+1-721\",\r\n" + 
			"        \"code\": \"SX \"\r\n" + 
			"    },\r\n" + 
			"    {\r\n" + 
			"        \"name\": \"Slovakia\",\r\n" + 
			"        \"prefix\": \"+421\",\r\n" + 
			"        \"code\": \"SK \"\r\n" + 
			"    },\r\n" + 
			"    {\r\n" + 
			"        \"name\": \"Slovenia\",\r\n" + 
			"        \"prefix\": \"+386\",\r\n" + 
			"        \"code\": \"SI \"\r\n" + 
			"    },\r\n" + 
			"    {\r\n" + 
			"        \"name\": \"Solomon Islands\",\r\n" + 
			"        \"prefix\": \"+677\",\r\n" + 
			"        \"code\": \"SB \"\r\n" + 
			"    },\r\n" + 
			"    {\r\n" + 
			"        \"name\": \"Somalia\",\r\n" + 
			"        \"prefix\": \"+252\",\r\n" + 
			"        \"code\": \"SO \"\r\n" + 
			"    },\r\n" + 
			"    {\r\n" + 
			"        \"name\": \"South Africa\",\r\n" + 
			"        \"prefix\": \"+27\",\r\n" + 
			"        \"code\": \"ZA \"\r\n" + 
			"    },\r\n" + 
			"    {\r\n" + 
			"        \"name\": \"South Korea\",\r\n" + 
			"        \"prefix\": \"+82\",\r\n" + 
			"        \"code\": \"KR \"\r\n" + 
			"    },\r\n" + 
			"    {\r\n" + 
			"        \"name\": \"South Sudan\",\r\n" + 
			"        \"prefix\": \"+211\",\r\n" + 
			"        \"code\": \"SS \"\r\n" + 
			"    },\r\n" + 
			"    {\r\n" + 
			"        \"name\": \"Spain\",\r\n" + 
			"        \"prefix\": \"+34\",\r\n" + 
			"        \"code\": \"ES \"\r\n" + 
			"    },\r\n" + 
			"    {\r\n" + 
			"        \"name\": \"Sri Lanka\",\r\n" + 
			"        \"prefix\": \"+94\",\r\n" + 
			"        \"code\": \"LK \"\r\n" + 
			"    },\r\n" + 
			"    {\r\n" + 
			"        \"name\": \"Saint Helena\",\r\n" + 
			"        \"prefix\": \"+290\",\r\n" + 
			"        \"code\": \"SH \"\r\n" + 
			"    },\r\n" + 
			"    {\r\n" + 
			"        \"name\": \"Saint Kitts and Nevis\",\r\n" + 
			"        \"prefix\": \"+1-869\",\r\n" + 
			"        \"code\": \"KN \"\r\n" + 
			"    },\r\n" + 
			"    {\r\n" + 
			"        \"name\": \"Saint Lucia\",\r\n" + 
			"        \"prefix\": \"+1-758\",\r\n" + 
			"        \"code\": \"LC \"\r\n" + 
			"    },\r\n" + 
			"    {\r\n" + 
			"        \"name\": \"Saint Martin\",\r\n" + 
			"        \"prefix\": \"+590\",\r\n" + 
			"        \"code\": \"MF \"\r\n" + 
			"    },\r\n" + 
			"    {\r\n" + 
			"        \"name\": \"Saint Pierre and Miquelon\",\r\n" + 
			"        \"prefix\": \"+508\",\r\n" + 
			"        \"code\": \"PM \"\r\n" + 
			"    },\r\n" + 
			"    {\r\n" + 
			"        \"name\": \"Saint Vincent and the Grenadines\",\r\n" + 
			"        \"prefix\": \"+1-784\",\r\n" + 
			"        \"code\": \"VC \"\r\n" + 
			"    },\r\n" + 
			"    {\r\n" + 
			"        \"name\": \"Sudan\",\r\n" + 
			"        \"prefix\": \"+249\",\r\n" + 
			"        \"code\": \"SD \"\r\n" + 
			"    },\r\n" + 
			"    {\r\n" + 
			"        \"name\": \"Suriname\",\r\n" + 
			"        \"prefix\": \"+597\",\r\n" + 
			"        \"code\": \"SR \"\r\n" + 
			"    },\r\n" + 
			"    {\r\n" + 
			"        \"name\": \"Svalbard and Jan Mayen\",\r\n" + 
			"        \"prefix\": \"+47\",\r\n" + 
			"        \"code\": \"SJ \"\r\n" + 
			"    },\r\n" + 
			"    {\r\n" + 
			"        \"name\": \"Swaziland\",\r\n" + 
			"        \"prefix\": \"+268\",\r\n" + 
			"        \"code\": \"SZ \"\r\n" + 
			"    },\r\n" + 
			"    {\r\n" + 
			"        \"name\": \"Sweden\",\r\n" + 
			"        \"prefix\": \"+46\",\r\n" + 
			"        \"code\": \"SE \"\r\n" + 
			"    },\r\n" + 
			"    {\r\n" + 
			"        \"name\": \"Switzerland\",\r\n" + 
			"        \"prefix\": \"+41\",\r\n" + 
			"        \"code\": \"CH \"\r\n" + 
			"    },\r\n" + 
			"    {\r\n" + 
			"        \"name\": \"Syria\",\r\n" + 
			"        \"prefix\": \"+963\",\r\n" + 
			"        \"code\": \"SY \"\r\n" + 
			"    },\r\n" + 
			"    {\r\n" + 
			"        \"name\": \"Taiwan\",\r\n" + 
			"        \"prefix\": \"+886\",\r\n" + 
			"        \"code\": \"TW \"\r\n" + 
			"    },\r\n" + 
			"    {\r\n" + 
			"        \"name\": \"Tajikistan\",\r\n" + 
			"        \"prefix\": \"+992\",\r\n" + 
			"        \"code\": \"TJ \"\r\n" + 
			"    },\r\n" + 
			"    {\r\n" + 
			"        \"name\": \"Tanzania\",\r\n" + 
			"        \"prefix\": \"+255\",\r\n" + 
			"        \"code\": \"TZ \"\r\n" + 
			"    },\r\n" + 
			"    {\r\n" + 
			"        \"name\": \"Thailand\",\r\n" + 
			"        \"prefix\": \"+66\",\r\n" + 
			"        \"code\": \"TH \"\r\n" + 
			"    },\r\n" + 
			"    {\r\n" + 
			"        \"name\": \"Togo\",\r\n" + 
			"        \"prefix\": \"+228\",\r\n" + 
			"        \"code\": \"TG \"\r\n" + 
			"    },\r\n" + 
			"    {\r\n" + 
			"        \"name\": \"Tokelau\",\r\n" + 
			"        \"prefix\": \"+690\",\r\n" + 
			"        \"code\": \"TK \"\r\n" + 
			"    },\r\n" + 
			"    {\r\n" + 
			"        \"name\": \"Tonga\",\r\n" + 
			"        \"prefix\": \"+676\",\r\n" + 
			"        \"code\": \"TO \"\r\n" + 
			"    },\r\n" + 
			"    {\r\n" + 
			"        \"name\": \"Trinidad and Tobago\",\r\n" + 
			"        \"prefix\": \"+1-868\",\r\n" + 
			"        \"code\": \"TT \"\r\n" + 
			"    },\r\n" + 
			"    {\r\n" + 
			"        \"name\": \"Tunisia\",\r\n" + 
			"        \"prefix\": \"+216\",\r\n" + 
			"        \"code\": \"TN \"\r\n" + 
			"    },\r\n" + 
			"    {\r\n" + 
			"        \"name\": \"Turkey\",\r\n" + 
			"        \"prefix\": \"+90\",\r\n" + 
			"        \"code\": \"TR \"\r\n" + 
			"    },\r\n" + 
			"    {\r\n" + 
			"        \"name\": \"Turkmenistan\",\r\n" + 
			"        \"prefix\": \"+993\",\r\n" + 
			"        \"code\": \"TM \"\r\n" + 
			"    },\r\n" + 
			"    {\r\n" + 
			"        \"name\": \"Turks and Caicos Islands\",\r\n" + 
			"        \"prefix\": \"+1-649\",\r\n" + 
			"        \"code\": \"TC \"\r\n" + 
			"    },\r\n" + 
			"    {\r\n" + 
			"        \"name\": \"Tuvalu\",\r\n" + 
			"        \"prefix\": \"+688\",\r\n" + 
			"        \"code\": \"TV \"\r\n" + 
			"    },\r\n" + 
			"    {\r\n" + 
			"        \"name\": \"United Arab Emirates\",\r\n" + 
			"        \"prefix\": \"+971\",\r\n" + 
			"        \"code\": \"AE \"\r\n" + 
			"    },\r\n" + 
			"    {\r\n" + 
			"        \"name\": \"Uganda\",\r\n" + 
			"        \"prefix\": \"+256\",\r\n" + 
			"        \"code\": \"UG \"\r\n" + 
			"    },\r\n" + 
			"    {\r\n" + 
			"        \"name\": \"United Kingdom\",\r\n" + 
			"        \"prefix\": \"+44\",\r\n" + 
			"        \"code\": \"GB \"\r\n" + 
			"    },\r\n" + 
			"    {\r\n" + 
			"        \"name\": \"Ukraine\",\r\n" + 
			"        \"prefix\": \"+380\",\r\n" + 
			"        \"code\": \"UA \"\r\n" + 
			"    },\r\n" + 
			"    {\r\n" + 
			"        \"name\": \"Uruguay\",\r\n" + 
			"        \"prefix\": \"+598\",\r\n" + 
			"        \"code\": \"UY \"\r\n" + 
			"    },\r\n" + 
			"    {\r\n" + 
			"        \"name\": \"United States\",\r\n" + 
			"        \"prefix\": \"+1\",\r\n" + 
			"        \"code\": \"US \"\r\n" + 
			"    },\r\n" + 
			"    {\r\n" + 
			"        \"name\": \"Uzbekistan\",\r\n" + 
			"        \"prefix\": \"+998\",\r\n" + 
			"        \"code\": \"UZ \"\r\n" + 
			"    },\r\n" + 
			"    {\r\n" + 
			"        \"name\": \"Vanuatu\",\r\n" + 
			"        \"prefix\": \"+678\",\r\n" + 
			"        \"code\": \"VU \"\r\n" + 
			"    },\r\n" + 
			"    {\r\n" + 
			"        \"name\": \"Vatican\",\r\n" + 
			"        \"prefix\": \"+379\",\r\n" + 
			"        \"code\": \"VA \"\r\n" + 
			"    },\r\n" + 
			"    {\r\n" + 
			"        \"name\": \"Venezuela\",\r\n" + 
			"        \"prefix\": \"+58\",\r\n" + 
			"        \"code\": \"VE \"\r\n" + 
			"    },\r\n" + 
			"    {\r\n" + 
			"        \"name\": \"Vietnam\",\r\n" + 
			"        \"prefix\": \"+84\",\r\n" + 
			"        \"code\": \"VN \"\r\n" + 
			"    },\r\n" + 
			"    {\r\n" + 
			"        \"name\": \"U.S. Virgin Islands\",\r\n" + 
			"        \"prefix\": \"+1-340\",\r\n" + 
			"        \"code\": \"VI \"\r\n" + 
			"    },\r\n" + 
			"    {\r\n" + 
			"        \"name\": \"Wallis and Futuna\",\r\n" + 
			"        \"prefix\": \"+681\",\r\n" + 
			"        \"code\": \"WF \"\r\n" + 
			"    },\r\n" + 
			"    {\r\n" + 
			"        \"name\": \"Western Sahara\",\r\n" + 
			"        \"prefix\": \"+212\",\r\n" + 
			"        \"code\": \"EH \"\r\n" + 
			"    },\r\n" + 
			"    {\r\n" + 
			"        \"name\": \"Yemen\",\r\n" + 
			"        \"prefix\": \"+967\",\r\n" + 
			"        \"code\": \"YE \"\r\n" + 
			"    },\r\n" + 
			"    {\r\n" + 
			"        \"name\": \"Zambia\",\r\n" + 
			"        \"prefix\": \"+260\",\r\n" + 
			"        \"code\": \"ZM \"\r\n" + 
			"    },\r\n" + 
			"    {\r\n" + 
			"        \"name\": \"Zimbabwe\",\r\n" + 
			"        \"prefix\": \"+263\",\r\n" + 
			"        \"code\": \"ZW \"\r\n" + 
			"    }\r\n" + 
			"]";

}
