package pe.olympus.constants;

public class UserStatus {
	
	public static final int VERIFICATION_PENDING = 1;
	public static final int ACTIVE = 2;
	public static final int BLOCKED = 3;
	public static final int SUSPENDED = 4;

}
