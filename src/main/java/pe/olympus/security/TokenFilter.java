package pe.olympus.security;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
import org.springframework.web.filter.OncePerRequestFilter;

public class TokenFilter extends OncePerRequestFilter  {
	
//	private final Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Override
	protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws IOException, ServletException {
			
		RequestWrapper wrappedRequest = new RequestWrapper((HttpServletRequest) request);
		ResponseWrapper wrappedResponse = new ResponseWrapper((HttpServletResponse) response);
		filterChain.doFilter(wrappedRequest, wrappedResponse);
		
		// ACA COLOCAR EL NUEVO RESPONSE
		String newContent = wrappedResponse.getCaptureAsString();
		
//		String newContent = "{\"test\":\"test\"}";
//		response.setContentLength(newContent .length());
		response.getWriter().write(newContent);
	}    
}
