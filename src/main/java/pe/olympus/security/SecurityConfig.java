package pe.olympus.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import com.fasterxml.jackson.databind.ObjectMapper;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(securedEnabled = true, jsr250Enabled = true, prePostEnabled = true)
public class SecurityConfig extends WebSecurityConfigurerAdapter {
 
	@Autowired
	ObjectMapper mapper;
	
	@Bean
	public TokenFilter tokenAuthenticationFilter() {
		return new TokenFilter();
	}
		
	@Override
    protected void configure(HttpSecurity http) throws Exception {
		
		http.addFilterBefore(tokenAuthenticationFilter(), UsernamePasswordAuthenticationFilter.class);
		
		http
//		.cors()
//		.and()
		.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
		.and()
		.csrf().disable()
		.formLogin().disable()
		.httpBasic().disable()
		.exceptionHandling()
//		.authenticationEntryPoint(restAuthenticationEntryPoint())
		.and().authorizeRequests()		
		.antMatchers("/**").permitAll();
//		.antMatchers("/users/login/validation").permitAll()
//		.antMatchers("/users/info").permitAll()
//		.antMatchers("/users/info/unlock").permitAll()
//		.antMatchers("/users/info/changePass").permitAll()
//		.antMatchers("/applications/subscriptions/bind").permitAll()
//		.antMatchers("/applications/subscriptions/data").permitAll()
//		.antMatchers("/applications/subscriptions/verify").permitAll()
//		.antMatchers("/applications/subscriptions/pagape").permitAll()
//		.antMatchers("/applications/**").authenticated()
//		.antMatchers("/applications/subscriptions/**").authenticated()
//		.antMatchers("/subscriptions/**").authenticated()
//		.antMatchers("/users/**").authenticated();
	
		
	
    }

}